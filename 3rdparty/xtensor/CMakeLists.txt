SET(XTENSOR_INCLUDE_DIR ${CMAKE_BINARY_DIR}/3rdparty/include)

# Arguments to use for running cmake ...
add_library(xtensor INTERFACE)
target_include_directories(xtensor INTERFACE ${XTENSOR_INCLUDE_DIR})
target_compile_definitions(xtensor INTERFACE XTENSOR_USE_XSIMD=1 XTENSOR_OPENMP_TRESHOLD=1000)
target_link_libraries(xtensor INTERFACE xsimd xtl)

IF(NOT EXISTS ${CMAKE_BINARY_DIR}/3rdparty/include/xtensor/xtensor.hpp)
	get_target_property(xtl_DIR xtl INTERFACE_INCLUDE_DIRECTORIES)
	get_target_property(xsimd_DIR xsimd INTERFACE_INCLUDE_DIRECTORIES)
	message(STATUS "Using xtl from "  ${xtl_DIR})
	message(STATUS "Using xsimd from "  ${xsimd_DIR})

	include(ExternalProject)
	SET(xtensor_cmake_args 
		-DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/3rdparty
		-DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}
		-Dxtl_DIR=${xtl_DIR}
		-DXTENSOR_USE_XSIMD=1
		-DXTENSOR_USE_OPENMP=1
		-Dxsimd_DIR=${xsimd_DIR}
		)

	ExternalProject_Add( xtensor-dl
	PREFIX ""
	GIT_REPOSITORY https://github.com/xtensor-stack/xtensor.git
	GIT_TAG 0.24.6
	# Configuration paramenters
	CMAKE_ARGS ${xtensor_cmake_args}
	EXCLUDE_FROM_ALL TRUE
	DEPENDS xtl xsimd
	)

	add_dependencies(xtensor xtensor-dl)
	message(STATUS "xtensor downloaded to ${XTENSOR_INCLUDE_DIR}/xtensor")
ENDIF()
