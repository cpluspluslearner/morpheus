#pragma once
#include <Qt>

// #if QT_VERSION < 0x040600
//     inline QIcon QThemedIcon(QString a, QIcon b) { return b; };
// #else
//     inline QIcon QThemedIcon(QString a, QIcon b) { return QIcon::fromTheme(a,b); };
// #endif

#if QT_VERSION < QT_VERSION_CHECK(5,14,0)
#include <QString>
#include <QTextStream>
namespace Qt {
#ifdef Q_OS_WIN 
	inline QTextStream& endl(QTextStream &stream) { stream << "\r\n"; stream.flush(); return stream; }
#else
	inline QTextStream& endl(QTextStream &stream) { stream << "\n"; stream.flush(); return stream; }
#endif
	using SplitBehavior = QString::SplitBehavior;
	const auto KeepEmptyParts = QString::KeepEmptyParts;
	const auto SkipEmptyParts = QString::SkipEmptyParts;
};

#endif
