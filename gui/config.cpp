#include "config.h"
#include "version.h"
#include "job_queue.h"

#ifdef USE_QWebEngine
	#include "network_schemes.h"
	#include <QWebEngineProfile>
#endif


config* config::instance = 0;

//------------------------------------------------------------------------------
config::~config() {
	job_queue->deleteLater();
	job_queue_thread->deleteLater();
	if (db.isOpen())
		db.close();
//     QSqlDatabase::removeDatabase("MorpheusJobDB");
}

config::config() : QObject(), helpEngine(NULL) {
	/* Restore Configuration setting from QSettings file
	 * Majority of settings are shared in the Morpheus.conf.
	 * Queue specific settings are stored in a MORPHEUS_BINARY_STRING specific configuration file
	 */
    QSettings settings;
	
	QString oD_default = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
	oD_default +="/morpheus";
    settings.beginGroup("simulation");
        app.general_outputDir       = settings.value("outputDir", oD_default).toString();
    settings.endGroup();


    settings.beginGroup("preferences");
		app.preference_allow_feedback = settings.value("allow_feedback", false).toBool();
        app.preference_stdout_limit = settings.value("stdout_limit", 10).toInt();
        app.preference_max_recent_files = settings.value("max_recent_files", 10).toInt();
        app.preference_jobqueue_interval = settings.value("jobqueue_interval", 2500).toInt();
        app.preference_jobqueue_interval_remote = settings.value("jobqueue_interval_remote", 10000).toInt();
    settings.endGroup();

#ifdef Q_OS_WIN32
        QString exec_default = QString(MORPHEUS_BINARY_STRING) +".exe";
        QString gnuplot_default = settings.value("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\gnuplot.exe", "").toString();
        QString ffmpeg_default =  settings.value("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\ffmpeg.exe", "").toString();
#else
		
		QString exec_default = QString(MORPHEUS_BINARY_STRING);
		QString gnuplot_default = "";
        QString ffmpeg_default = "";
#endif

	settings.beginGroup("local");
		gnuplot_default = settings.value("GnuPlotExecutable", gnuplot_default).toString();
		ffmpeg_default = settings.value("FFmpegExecutable", ffmpeg_default).toString();
	settings.endGroup();
	
	QString app_name = MORPHEUS_BINARY_STRING; app_name[0] = 'M';
	QSettings local_settings("Morpheus",app_name);
	local_settings.beginGroup("local");
		app.local_executable        = local_settings.value("executable",exec_default).toString();
		app.local_GnuPlot_executable = local_settings.value("GnuPlotExecutable",gnuplot_default).toString();
		app.local_FFmpeg_executable = local_settings.value("FFmpegExecutable",ffmpeg_default).toString();
		app.local_maxConcurrentJobs = local_settings.value("maxConcurrentJobs", settings.value("maxConcurrentJobs", 2)).toInt();
		app.local_maxThreads        = local_settings.value("maxThreads",settings.value("maxThreads", -1)).toInt();
		if ( ! local_settings.contains("maxThreads_reset")) {
			if (app.local_maxThreads == 1) {
				app.local_maxThreads = -1;
				local_settings.setValue("maxThreads", -1);
			}
			local_settings.setValue("maxThreads_reset", true);
		}
		app.local_timeOut           = local_settings.value("timeOut", settings.value("timeOut", 999)).toInt();
	local_settings.endGroup();

	settings.beginGroup("remote");
		QString local_username = QString( getenv("USER") ); // best guess is that remote username is equal to local username
		app.remote_user             = settings.value("user",local_username).toString();
		app.remote_host             = settings.value("host","").toString();
		app.remote_simDir           = settings.value("simDir","simulation").toString();
		app.remote_executable       = settings.value("executable",MORPHEUS_BINARY_STRING).toString();
		app.remote_GnuPlot_executable = settings.value("GnuPlotExecutable","").toString();
		app.remote_maxThreads       = settings.value("maxThreads",4).toInt();
		app.remote_dataSyncType     = settings.value("dataSyncType","continuous").toString();
	settings.endGroup();

	current_model = -1;
	qRegisterMetaType<SharedMorphModel>("SharedMorphModel");

/*
       Initialize a SQLite database for jobs and sweeps
*/
	try {
		db = QSqlDatabase::addDatabase("QSQLITE"); // ,"MorpheusJobDB"); <-- DB creation fails when connection name is set.
		if(!db.isValid() ) throw;
		
		QString data_base_file_name = "morpheus.db.sqlite";
		// The data location has been changed between qt4 and qt5, thus we have to migrate the old data base manually
// 		QDir job_db_path(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+"/data/Morpheus/Morpheus");
		QDir old_location = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+"/data/Morpheus/Morpheus";
		QDir old_location2 = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation)+"/Morpheus/Morpheus";
		QDir current_location = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation); // provides on Windows ~/AppData/Local/Morpheus
		current_location.mkpath(current_location.path());
		
		if (! QFile::exists(current_location.filePath(data_base_file_name))) {
			if (QFile::exists(old_location.filePath(data_base_file_name))) {
				QFile::copy(old_location.filePath(data_base_file_name), current_location.filePath(data_base_file_name) );
				qDebug() << "Migrating job db to " << current_location.filePath(data_base_file_name);
			}
			else if (QFile::exists(old_location2.filePath(data_base_file_name))) {
				QFile::copy(old_location2.filePath(data_base_file_name), current_location.filePath(data_base_file_name) );
				qDebug() << "Migrating job db to " << current_location.filePath(data_base_file_name);
			}
		}
		
		db.setDatabaseName(current_location.filePath("morpheus.db.sqlite"));
		qDebug() << "SQLite database path: "<< current_location  << data_base_file_name;
		db.setConnectOptions("QSQLITE_BUSY_TIMEOUT=1000");
		if (!db.open()) throw;
		db.exec("PRAGMA synchronous=NORMAL");
			
		// Check Database Version
		if (db.tables().contains("VersionHistory")) {
			QSqlQuery query;
			query.prepare("SELECT * FROM VersionHistory ORDER BY version DESC");
			bool ok=query.exec();
			if( !ok ){
				qDebug() << "Retrieval of database version failed: " << query.lastError();
				throw query.lastError().text();
			}
			ok = query.first();
			if( !ok ){
				qDebug() << "Retrieval of database version failed: " << query.lastError();
				throw query.lastError().text();
			}
			int cdb_version = query.value(0).toInt();
			if (cdb_version != data_base_version) {
				if (cdb_version < data_base_version) {
					qDebug() << "SQLite database has version "<< cdb_version <<". Expected version " << data_base_version << ".";
					throw QString("Mismatching database version");
				}
				else {
					qDebug() << "SQLite database has version "<< cdb_version <<". Expected version " << data_base_version << ".";
					throw QString("Mismatching database version");
				}
			}
		} 
		else {
			QSqlQuery query;
			query.prepare(
			"CREATE TABLE VersionHistory ("
				"version INTEGER PRIMARY KEY NOT NULL,"
				"upgraded DATE NOT NULL )"
			);
			bool ok = query.exec();
			if( !ok ){
				qDebug() << "Creating SQL table VersionHistory failed: " << query.lastError();
				throw query.lastError().text();
			}
			qDebug() << "Successfully created version history database";
			query.prepare( QString("INSERT INTO VersionHistory ( version , upgraded ) VALUES( %1 , date('now') )").arg(data_base_version) );
			ok = query.exec();
			if( !ok ){
				qDebug() << "Setting current database version in table VersionHistory failed: " << query.lastError();
				throw query.lastError().text();
			}
			qDebug() << "Successfully set default database version";
			
		}
	}
	catch (const QString& e) {
		QMessageBox::critical(0,
			"Invalid database",
			QString("Invalid SQL database connection.\n")+e +"\nClick Cancel to exit.",
			QMessageBox::Cancel );
		throw("Unable to create database connection");
		qApp->exit();
	}
	
	catch (...) {
		QMessageBox::critical(0,
			qApp->tr("Invalid database"),
			qApp->tr("Invalid SQL database connection.\nClick Cancel to exit."),
			QMessageBox::Cancel );
		throw("Unable to create database connection");
		qApp->exit();
	}

/*		std::cout << "database NOT open" << std::endl;
		QMessageBox::critical(0, qApp->tr("Cannot open database"),
		qApp->tr("Unable to establish a database connection.\n"
		         "Morpheus needs SQLite support to store job information.\n\n"
		         "Click Cancel to exit."), QMessageBox::Cancel);
		qApp->quit();
	} */
	
	if ( ! db.tables().contains("jobs")) {
		qDebug() << "creating Job DataBase";
		QSqlQuery query(db);
		query.prepare(
			"CREATE TABLE IF NOT EXISTS jobs ("
			"id INTEGER PRIMARY KEY, "
			// simulation
			"processPid INTEGER DEFAULT -1,"
			"processThreads INTEGER DEFAULT 1, "
			"processState INTEGER DEFAULT -1, "
			"processResource INTEGER DEFAULT -1, "
			"simTitle VARCHAR(255) DEFAULT \"\" , "
			"simXMLname VARCHAR(255) DEFAULT \"\", "
			"simDirectory VARCHAR(255) DEFAULT \"\", "
			// time
			"timeStart INTEGER DEFAULT 0, "
			"timeStop INTEGER DEFAULT 1, "
			"timeCurrent INTEGER DEFAULT 0, "
			"timeExec INTEGER DEFAULT 0, "
			// remote 
			"remoteTask INTEGER DEFAULT 0 )"
		);
		bool ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL table jobs failed: " << query.lastError();
		}
		
// 		query.prepare("CREATE INDEX jobsweeps ON jobs(sweepId) ");
// 		ok = query.exec();
// 		if( !ok ){
// 			qDebug() << "Creating SQL index jobs.sweepId failed: " << query.lastError();
// 		}
// 		
		query.prepare(
			"CREATE TABLE IF NOT EXISTS sweeps ("
				"id INTEGER PRIMARY KEY, "
				"name VARCHAR(255) DEFAULT \"\", "
				"header TEXT DEFAULT \"\","
				"subDir VARCHAR(255) DEFAULT \"\" ,"
				"paramData BLOB"
			")"
		 );
		ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL table sweeps failed: " << query.lastError();
		}
		
		query.prepare(
			"CREATE TABLE IF NOT EXISTS sweep_jobs ("
				"id INTEGER DEFAULT NULL PRIMARY KEY , "
				"sweep REFERENCES sweeps(id), "
				"job REFERENCES jobs(id), "
				"paramSet TEXT DEFAULT \"\""
			")"
		);
		ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL table sweep_jobs failed: " << query.lastError();
		}
		query.prepare("CREATE INDEX swjo1 ON sweep_jobs(sweep) ");
		ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL index on sweep_jobs(sweep) failed: " << query.lastError();
		}
		query.prepare("CREATE UNIQUE INDEX swjo2 ON sweep_jobs(job) ");
		ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL index on sweep_jobs(job) failed: " << query.lastError();
		}

		query.prepare("CREATE  TRIGGER remove_job AFTER DELETE ON jobs BEGIN DELETE FROM sweep_jobs WHERE job=OLD.id; END; ");
		ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL trigger for removing jobs failed: " << query.lastError();
		}
		
		query.prepare(
			"CREATE TRIGGER remove_sweep_job AFTER DELETE ON sweep_jobs "
			"BEGIN "
				"DELETE FROM sweeps WHERE "
					"OLD.sweep=id "
					"AND NOT EXISTS (SELECT * FROM sweep_jobs WHERE sweep=OLD.sweep); "
			"END ");
				ok = query.exec();
		if( !ok ){
			qDebug() << "Creating SQL trigger for removing sweep_jobs failed: " << query.lastError();
		}
	}
// 	db.close();

	// Housekeeping
	QSqlQuery query(
		"DELETE FROM sweep_jobs " 
		"WHERE NOT EXISTS ( "
			"SELECT * FROM jobs "
			"WHERE jobs.id=sweep_jobs.job "
		");",
		db);
	bool ok = query.exec();
	if( !ok ){
		qDebug() << "Housekeeping of sweep_jobs failed: " << query.lastError();
		qDebug() << query.lastQuery();
	}
	
/**
  SELECT * FROM sweep_jobs
  WHERE NOT EXISTS (
  	SELECT * FROM jobs
  	WHERE jobs.id=sweep_jobs.job
  );
  */
	
	// Creating a Job Queue that runs in a separate thread ..
// 	qDebug() << "Main thread " << QThread::currentThreadId();
	job_queue_thread = new QThread();
	job_queue = new JobQueue();
	job_queue->moveToThread(job_queue_thread);
	connect( job_queue_thread, SIGNAL(started()), job_queue, SLOT(run()) );

	// 		connect( task, SIGNAL(workFinished()), thread, SLOT(quit()) );
	//automatically delete thread and task object when work is done:
	connect( job_queue_thread, SIGNAL(finished()), job_queue, SLOT(deleteLater()) );
	connect( job_queue_thread, SIGNAL(finished()), job_queue_thread, SLOT(deleteLater()) );

	job_queue_thread->start();
}

//------------------------------------------------------------------------------

config* config::getInstance() {
    if ( ! config::instance ) {
        config::instance = new config();
		// Attaching to Clipboard
		connect(QApplication::clipboard(), SIGNAL(dataChanged()), config::instance, SLOT(ClipBoardChanged()));
    }
    return config::instance;
}


//------------------------------------------------------------------------------

SharedMorphModel config::getModel() {
//    qDebug() << getInstance()->current_model << " " << getInstance()->openModels.size();
	if (getInstance()->openModels.empty())
		return SharedMorphModel();
	
    if (getInstance()->current_model > getInstance()->openModels.size() -1 || getInstance()->current_model < 0)
		return SharedMorphModel();
//         getInstance()->current_model = getInstance()->openModels.size() -1;
    return getInstance()->openModels[getInstance()->current_model];
}

// ---------------------------------------------------

QString config::getPathToExecutable(QString exec_name) {
	
	exec_name = exec_name.trimmed();
#ifdef Q_OS_WIN32
	if (!exec_name.endsWith(".exe"))
		exec_name.append(".exe");
#endif
	QFileInfo info;
	info.setFile(exec_name);
	if (info.exists() && info.isExecutable() && info.isFile()) {
// 		qDebug() << "Found executable " << info.filePath();
		return info.canonicalFilePath();
	}
	
	QString app_name = exec_name.split("/").last();

	info.setFile(QCoreApplication::applicationDirPath() + "/" + app_name);
	if (info.exists() && info.isExecutable()  && info.isFile()) {
// 		qDebug() << "Found executable " << info.filePath();
		return info.canonicalFilePath();
	}
	
	char* env_paths_c = getenv("PATH");
#ifdef Q_OS_WIN32
	QStringList env_paths = QString(env_paths_c).split(";");
#elif defined Q_OS_MACOS
	QStringList env_paths = QString(env_paths_c).split(":");
	QStringList homebrew_paths = { // Fallback Homebrew binary locations
		"/usr/local/bin", // macOS Intel
		"/opt/homebrew/bin", // Apple Silicon
		"/usr/local/Cellar/gnuplot-5.4.5/bin", // avoid issues with unlinked gnuplot Intel Silicon
		"/opt/homebrew/Cellar/gnuplot-5.4.5/bin" // avoid issues with unlinked gnuplot Apple Silicon
	};
	env_paths += homebrew_paths;
#else
	QStringList env_paths = QString(env_paths_c).split(":");
#endif


	
	Q_FOREACH (const QString& path, env_paths) {
		info.setFile(path + "/" + app_name);
		if (info.exists() && info.isExecutable()  && info.isFile()) {
// 			qDebug() << "Found executable " << info.filePath();
			return info.canonicalFilePath();
		}
	}
	
	return "";
	
}

//------------------------------------------------------------------------------

QList< SharedMorphModel > config::getOpenModels() {
    return getInstance()->openModels;
}


JobQueue* config::getJobQueue() {
    Q_ASSERT(getInstance()->job_queue);
    return getInstance()->job_queue;
}

//------------------------------------------------------------------------------

int config::openModel(QString filepath) {
	if (filepath.isEmpty()) return -1;
	// if the model is already open, just switch to that model
	config* conf = getInstance();
	QString xmlFile = QFileInfo(filepath).absoluteFilePath();
	for (int i=0; i<conf->openModels.size(); i++) {
		if (xmlFile==conf->openModels[i]->xml_file.path) {
			emit conf->modelSelectionChanged(i);
			return i;
		}
	}

	SharedMorphModel m;
	try {
		m = SharedMorphModel( new MorphModel(xmlFile,conf));
	}
	catch(ModelException e) {
		// TODO activate this message ...
		QMessageBox::critical(qApp->activeWindow(),"Error opening morpheus model",e.message);
		return -1;
	}
	catch(QString e) {
		QMessageBox::critical(qApp->activeWindow(),"Error opening morpheus model",e);
		return -1; 
	}
	catch(...) {
		QMessageBox::critical(qApp->activeWindow(),"Error opening morpheus model","Unknown error");
		return -1; 
	}

	// substitude the last model if it was created from scratch and is still unchanged
	if ( ! conf->openModels.isEmpty() && conf->openModels.back()->isEmpty()) {
		closeModel(conf->openModels.size()-1,false);
	}
	
    conf->openModels.push_back(m);
    int new_index = conf->openModels.size()-1;
// 	qDebug() << "Added model " << new_index;
    addRecentFile(m->xml_file.path);
    emit conf->modelAdded(new_index);
	

    return new_index;
}

int config::importModel(SharedMorphModel model)
{
	config* conf = getInstance();
	
	int index = conf->openModels.indexOf(model);
	if ( index >= 0) {
		return index;
	}
	
	// substitude the last model if it was created from scratch and is still unchanged
	if ( ! conf->openModels.isEmpty() && conf->openModels.back()->isEmpty()) {
		closeModel(conf->openModels.size()-1, false);
	}
	
	model->setParent(conf);
	conf->openModels.push_back(model);
	index = conf->openModels.size()-1;
	emit conf->modelAdded(index);
	
	return index;
}


//------------------------------------------------------------------------------

int config::createModel(QString xml_path)
{
    config* conf = getInstance();

    if ( ! conf->openModels.isEmpty() &&  conf->openModels.back()->isEmpty()) {
        conf->openModels.back()->close();
        emit conf->modelClosing(conf->openModels.size()-1);
        conf->openModels.pop_back();
    }
    SharedMorphModel m;
	try {
		if (xml_path.isEmpty()) {
			m =  SharedMorphModel(new MorphModel(conf));
		}
		else {
			m = SharedMorphModel( new MorphModel(xml_path,conf));
			m->xml_file.path = "";
			m->xml_file.name = MorpheusXML::getNewModelName();
			m->rootNodeContr->saved();
		}
	}
	catch(ModelException e) {
		QMessageBox::critical(qApp->activeWindow(),"Error creating morpheus model",e.message ,QMessageBox::Ok,QMessageBox::NoButton);
		return -1;
	}
	catch(QString e) {
		QMessageBox::critical(qApp->activeWindow(),"Error creating morpheus model",e ,QMessageBox::Ok,QMessageBox::NoButton);
		return -1;
	}
	catch(...){
		QMessageBox::critical(qApp->activeWindow(),"Error opening morpheus model","Unknown error");
		return -1; 
	}
    int id = conf->openModels.size();
    conf->openModels.push_back(m);
    emit conf->modelAdded(id);

    return id;
}

//------------------------------------------------------------------------------

bool config::closeModel(int index, bool create_model)
{
// 	qDebug() << "Closing Model" << index;
	config* conf = getInstance();
	if (index == -1) index = conf->current_model;
	if (index >= conf->openModels.size()) return false;
	if (conf->openModels[index]->close()) {
		emit conf->modelClosing(index);
		conf->openModels.removeAt(index);
		
		if (conf->openModels.size()==0) {
			conf->switchModel(-1);
			if (create_model)
				conf->switchModel(createModel());
		}
		else if (index == conf->current_model) {
			// pick activate alternative model
			if (conf->current_model < conf->openModels.size())
				conf->switchModel(conf->current_model);
			else
				conf->switchModel(conf->current_model-1);
		}
		
	}
	else
		return false;
	return true;
}

//------------------------------------------------------------------------------

void config::switchModel(int index) {
	config* conf = config::getInstance();
// 	qDebug() << "Switch from model" << conf->current_model << "to model " << index;
	if (index == conf->current_model) return;
	conf->current_model = index;
	emit conf->modelSelectionChanged(conf->current_model);
}


//------------------------------------------------------------------------------

QSqlDatabase config::getDatabase()
{
	config* conf = getInstance();
	// Starting with Qt 5.11, sharing the same connection between threads is not allowed.
    // Use a dedicated connection for each thread requiring access to the database,
    // using the thread address as connection name.

    QSqlDatabase cnx;

    QString dbName = QStringLiteral("morpheus_db_%1").arg(qintptr(QThread::currentThreadId()), 0, 16);
    if(QSqlDatabase::contains(dbName))
    {
        cnx = QSqlDatabase::database(dbName);
    }
    else
    {
        cnx = QSqlDatabase::cloneDatabase(conf->db, dbName);

    }
    if ( !cnx.isOpen() && !cnx.open() ) {
		qDebug() << "DB connection creation error!";
	}
    return cnx;

	
// 	if ( ! conf->db.isOpen() )
// 		conf->db.open();
// 	return conf->db;
}


QString config::getVersion() {
	return QString(MORPHEUS_VERSION_STRING);
}

//------------------------------------------------------------------------------

const config::application& config::getApplication() {
    return getInstance()->app;
}

//------------------------------------------------------------------------------

const QList<QDomNode> config::getNodeCopies() {
    return getInstance()->xmlNodeCopies;
}

//------------------------------------------------------------------------------

void config::setApplication(application a) {

	config* conf = getInstance();
	if (a.remote_user != conf->app.remote_user || a.remote_host != conf->app.remote_host) {
		getInstance()->app = a;
		sshProxy().clearSessions();
	}
	else {
		getInstance()->app = a;
	}

	QSettings settings;
	settings.beginGroup("simulation");
		settings.setValue("outputDir", a.general_outputDir);
	settings.endGroup();

	settings.beginGroup("preferences");
		settings.setValue("allow_feedback",a.preference_allow_feedback);
		settings.setValue("stdout_limit", a.preference_stdout_limit);
		settings.setValue("max_recent_files", a.preference_max_recent_files);
		settings.setValue("jobqueue_interval", a.preference_jobqueue_interval);
		settings.setValue("jobqueue_interval_remote", a.preference_jobqueue_interval_remote);
	settings.endGroup();
	
	QString app_name = MORPHEUS_BINARY_STRING; app_name[0] = 'M';
	QSettings local_settings("Morpheus",app_name);
	local_settings.beginGroup("local");
		local_settings.setValue("executable",         a.local_executable);
		local_settings.setValue("GnuPlotExecutable",  a.local_GnuPlot_executable);
		local_settings.setValue("FFmpegExecutable",  a.local_FFmpeg_executable);
		local_settings.setValue("maxConcurrentJobs",  a.local_maxConcurrentJobs);
		local_settings.setValue("maxThreads",         a.local_maxThreads);
		local_settings.setValue("timeOut",            a.local_timeOut);
	local_settings.endGroup();

	local_settings.beginGroup("remote");
		local_settings.setValue("user",               a.remote_user);
		local_settings.setValue("host",               a.remote_host);

		local_settings.setValue("executable",         a.remote_executable);
		local_settings.setValue("GnuPlotExecutable",  a.remote_GnuPlot_executable);
		local_settings.setValue("maxThreads",        a.remote_maxThreads);

		local_settings.setValue("dataSyncType",       a.remote_dataSyncType);
		local_settings.setValue("simDir",             a.remote_simDir);
	local_settings.endGroup();

}


//------------------------------------------------------------------------------

void config::setComputeResource(QString resource) {
    getInstance()->app.general_resource = resource;
}

//------------------------------------------------------------------------------

void config::receiveNodeCopy(QDomNode nodeCopy) {
    xmlNodeCopies.push_front(nodeCopy);
    while(xmlNodeCopies.size() > MaxNodeCopies) {
        xmlNodeCopies.pop_back();
    }
}

void config::ClipBoardChanged() {
	auto mimeData = QApplication::clipboard()->mimeData();
	
	if (mimeData->hasText()) {
		clipBoard_Document.setContent(mimeData->text());
		if (!clipBoard_Document.firstChildElement().isNull()) {
			receiveNodeCopy(clipBoard_Document.firstChildElement());
		}
	}
}

//------------------------------------------------------------------------------
void config::openExamplesWebsite(){
	QDesktopServices::openUrl(QUrl("https://morpheus.gitlab.io/#examples"));
}
//------------------------------------------------------------------------------
void config::openMorpheusWebsite(){
	QDesktopServices::openUrl(QUrl("https://morpheus.gitlab.io"));
}

//------------------------------------------------------------------------------
void config::aboutModel()
{
	QString about;
	if (!getInstance()->getModel())
		return;
	QString title = getInstance()->getModel()->rootNodeContr->getModelDescr().title;
	QString details = getInstance()->getModel()->rootNodeContr->getModelDescr().details;


	about += "Model:\n " + (title.size()>0?title:"Unknown title") + (details.size()==0?"":"\n\nDetails:\n " + details) +"\n\nFile:\n " + getModel()->xml_file.path;

	QMessageBox msgBox(QMessageBox::Information, "About model",about,QMessageBox::Ok);
	msgBox.setIconPixmap(QPixmap(":/logo.png"));
	//msgBox.setParent(qApp->activeWindow());
	msgBox.exec();
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

void config::aboutPlatform()
{
	
	QDate date = QDate::currentDate();
	
	QString header = "<img style='float:left' src='qrc://logo.png'/> <h1 style='margin-right:40px' align='center'>Morpheus</h1>";

	QString info  = "<h3 align='center'>Modeling and simulation environment for multi-scale and multicellular systems biology</h3>"
					"<p align='center'>Version "+QString(MORPHEUS_VERSION_STRING)+", revision " + QString(MORPHEUS_REVISION_STRING) + "<br/>"
					"Resource identification: Morpheus (RRID:SCR_014975) <br/><br/>"
					
					"Developed by Jörn Starruß, Walter de Back, Lutz Brusch, Cedric Unverricht,<br/> Robert Müller and Diego Jahn<br/>"
					"Copyright 2009-"+ QString::number( date.year() )+", Technische Universität Dresden.<br><br>"
					"More information:<br><a href=\"https://morpheus.gitlab.io\">morpheus.gitlab.io</a></p>"
					"<p style='font-size:small'>"
// 					"<b>Contributors</b><br/>Cedric Unverricht, Robert Müller, Diego Jahn, Gerhard Burger, Margriet Palm,  Osvaldo Chara, Martin Lunze<br/>"
					"<b>Disclaimer</b><br/>Non-commercial use: Morpheus (the Software) is distributed for academic use and cannot be used for commercial gain without explicitly written agreement by the Developers. No warranty: The Software is provided \"as is\" without warranty of any kind, either express or implied, including without limitation any implied warranties of condition, uninterrupted use, merchantability, fitness for a particular purpose, or non-infringement. No liability: The Developers do not accept any liability for any direct, indirect, incidential, special, exemplary or consequential damages arising in any way out of the use of the Software.</p>";


	QMessageBox msgBox;
	msgBox.setText(header);
	msgBox.setInformativeText(info);
	msgBox.setStandardButtons(QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Ok);    
	msgBox.setTextFormat(Qt::RichText);
	msgBox.exec();
}


QHelpEngine* config::getHelpEngine(bool lock)
{
	config* conf = getInstance();
	if (!conf->helpEngine) {
		if (lock)
			conf->change_lock.lock();
		if (!conf->helpEngine) {
			QApplication::applicationDirPath();
			
			QStringList doc_path;
			doc_path <<  QApplication::applicationDirPath()
						<< QApplication::applicationDirPath() + "/appdoc"
						<< QApplication::applicationDirPath() + "/../share/morpheus"
						<< QApplication::applicationDirPath() + "/../.local/share/morpheus"
						<< QApplication::applicationDirPath() + "/../Resources"; // for Mac app bundle
			QString help_path;
			QString help_file_name =  MORPHEUS_BINARY_STRING ".qch";

			for(const QString& p:  doc_path) {
				qDebug() << "Testing "  << p + "/" + help_file_name;
				if (QFile::exists(p+"/"+help_file_name))
					help_path = QDir(p).canonicalPath()+"/"+help_file_name;
			}
			
			if (help_path.isEmpty()) {
				qDebug() << "Help engine setup failed. Unable to locate " << help_file_name << ".";
				conf->helpEngine = new QHelpEngine("");
			}
			else {
				qDebug() << "Documentation located at "  << help_path;
				QDir data_path(QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation));
				QString docu_collection = data_path.filePath(MORPHEUS_BINARY_STRING  ".qhc");
				
				conf->helpEngine = new QHelpEngine(docu_collection, conf);
				if (conf->helpEngine->setupData() == false) {
					qDebug() << "Help engine setup failed";
				}
				QString docu_name = "org.morpheus.UserDocu";
				qDebug() << conf->helpEngine->registeredDocumentations();
				if (!conf->helpEngine->registeredDocumentations().contains(docu_name)) {
					if (!conf->helpEngine->registerDocumentation(help_path)) {
						qDebug() << "Unable to register documentation";
						qDebug() << conf->helpEngine->error();
					}
				}
			}
		}
		if (lock)
			conf->change_lock.unlock();
	}
	return conf->helpEngine;
}

ExtendedNetworkAccessManager* config::getNetwork() {
	config* conf = getInstance();
	if (!conf->network) {
		conf->change_lock.lock();
		if (!conf->network) {
			conf->network = new ExtendedNetworkAccessManager(conf, getHelpEngine(false));
#ifdef USE_QWebEngine
			auto *help_handler = new HelpNetworkScheme(conf->network, conf);
			QWebEngineProfile::defaultProfile()->installUrlSchemeHandler(HelpNetworkScheme::scheme(), help_handler);
// 			auto *qrc_handler = new QtRessourceScheme(conf->network, conf);
// 			QWebEngineProfile::defaultProfile()->installUrlSchemeHandler(QtRessourceScheme::scheme(), qrc_handler);
#endif
		}
		conf->change_lock.unlock();
	}
	return conf->network;
}

void config::aboutHelp() {
	
	QDialog* help_box = new QDialog(0,Qt::Dialog );

	QHelpEngine* help = getHelpEngine();

	// "org.doxygen.Project"
	help_box->setModal(true);
	QBoxLayout* help_layout = new QBoxLayout(QBoxLayout::TopToBottom);
	help_box->setLayout(help_layout);
	
	QSplitter *helpPanel = new QSplitter(Qt::Vertical);
	QHelpContentWidget* contentBrowser = help->contentWidget();
	helpPanel->insertWidget(0, contentBrowser);
	help_layout->addWidget(helpPanel);
	
	help_box->exec();
}


//------------------------------------------------------------------------------

void config::addRecentFile(QString filePath)
{
    QStringList files = QSettings().value("recentFileList").toStringList();
    files.removeAll(filePath);
    files.prepend(filePath);
    while(files.size() > config::getApplication().preference_max_recent_files)
        files.removeLast();
    QSettings().setValue("recentFileList", files);

    emit config::getInstance()->newRecentFile();
}
