#include "morpheus_xml.h"
#include <errno.h>

MorpheusXML::MorpheusXML() {
    name = getNewModelName();
    path ="";
    is_plain_model = true;
	is_zipped = false;
    QDomDocument cpmDoc( "cpmDocument" );
    QString str("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><MorpheusModel version=\"95782\"></MorpheusModel>");
    cpmDoc.setContent(str);
    xmlDocument = cpmDoc;
};

MorpheusXML::MorpheusXML(QDomDocument model) {
    name = getNewModelName();
    path ="";
    is_plain_model = false;
	is_zipped = false;
    xmlDocument = model;
	CommentsToDisabled();
};


MorpheusXML::MorpheusXML(QByteArray data) {
    name = getNewModelName();
    path ="";
    is_plain_model = false;
	is_zipped = false;
	QString error_msg;
	int error_line;
	int error_column;
    if (!xmlDocument.setContent(data,&error_msg,&error_line,&error_column)) {
		throw QString("Unable to create internal DOM structure.!\n\n%1 at line %2, column %3.").arg(error_msg).arg(error_line).arg(error_column);
	}
	CommentsToDisabled();
};

//------------------------------------------------------------------------------

MorpheusXML::MorpheusXML(QString xmlFile) {
    QDomDocument cpmDoc( "cpmDocument" );
	
	
	QMimeDatabase db;
    QMimeType type = db.mimeTypeForFile(xmlFile);
	
	if (type.name()=="application/gzip" || QFileInfo(xmlFile).suffix() == "gz" || QFileInfo(xmlFile).suffix() == "xmlz" ) {
		gzFile gzDoc;
#ifdef WIN32
		QByteArray native_string = QDir::toNativeSeparators(xmlFile).toLatin1();
#else
		QByteArray native_string = xmlFile.toUtf8();
#endif
		gzDoc = gzopen(native_string.data(), "rb");
		if (gzDoc == NULL ) {
			qDebug() << "Error " << errno << ": " << strerror(errno);
			throw QString("Cannot open file '%1', possibly not a valid gzip file.").arg(qPrintable(xmlFile));
		}
		
		QByteArray data;
		char buff[4097];
		int i; 
		while ((i = gzread(gzDoc, &buff, 4096)) > 0) {
			buff[i] = '\0';
			data.append(buff);
		}
		gzclose(gzDoc);
		
		QString error_msg; int error_line, error_column;
		if ( ! cpmDoc.setContent(data,true, &error_msg, &error_line, &error_column)) {
			throw QString("Unable to create internal DOM structure from \"%1\"!\n\n%2 at line %3, column %4.").arg(xmlFile,error_msg).arg(error_line).arg(error_column);
		}
		path = xmlFile;
		name = QFileInfo(path).fileName();
		is_zipped = true;
	}
	else {
		QFile file(xmlFile);
		if ( ! file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
			throw QString("Cannot open file %1.").arg(xmlFile);
		}
		QString error_msg; int error_line, error_column;
		if ( ! cpmDoc.setContent(&file,true, &error_msg, &error_line, &error_column)) {
			file.close();
			throw QString("Unable to create internal DOM structure from \"%1\"!\n\n%2 at line %3, column %4.").arg(xmlFile,error_msg).arg(error_line).arg(error_column);
		}
		file.close();
		path = QFileInfo(xmlFile).absoluteFilePath();
		name = QFileInfo(path).fileName();
		is_zipped = false;
	}
	qDebug() << "Reading of XML-Document succesfully finished!";

	xmlDocument = cpmDoc;
	CommentsToDisabled();
	is_plain_model = false;
};


void replaceNestedDisabled(QDomElement el) {
	auto childs = el.childNodes();
	for (int i=0; i<childs.size(); i++) {
		if (childs.at(i).nodeType() == QDomNode::ElementNode) {
			auto elc  = childs.at(i).toElement();
			if (elc.nodeName() == "Disabled") {
				elc = elc.firstChildElement();
				el.replaceChild(elc,childs.at(i).toElement());
				elc.setAttribute("internal:disabled","true");
			}
			replaceNestedDisabled(elc);
		}
	}
}

void MorpheusXML::CommentsToDisabled() {
	auto replace_disabled = [&](QDomNode element, auto &&replacer) -> void {
		auto childs = element.childNodes();
		for (int i=0; i<childs.size(); i++) {
			if (childs.at(i).isComment()) {
				// try to convert to disabled node
				auto comment = childs.at(i).toComment();
				QDomDocument doc;
				doc.setContent(comment.data());
				// DocumentElement must be <Disabled>
				// the first child below is the real disabled node ...
				auto disabledNode = doc.documentElement();
				if (disabledNode.nodeName() == "Disabled") {
					auto dataNode = disabledNode.firstChild();
					dataNode.toElement().setAttribute("internal:disabled", "true");
					// iteratively also replace all nested Disabled nodes
					replaceNestedDisabled(dataNode.toElement());
					comment.parentNode().replaceChild(dataNode, comment);
					comment.clear();
				}
			}
			else if (childs.at(i).isElement()) {
				replacer(childs.at(i).toElement(), replacer);
			}
		}
	};
	replace_disabled(xmlDocument.documentElement(), replace_disabled);
}

//------------------------------------------------------------------------------

bool MorpheusXML::save(QString fileName, bool zip) const {
	QString outputXML = fileName;
	// pull the focus, such that all edit operations finish
// 	if (qApp->focusWidget())
// 		qApp->focusWidget()->clearFocus();

	if (zip) {
		gzFile gzDoc;
#ifdef WIN32
		QByteArray native_string = QDir::toNativeSeparators(outputXML).toLatin1();
#else
		QByteArray native_string = outputXML.toUtf8();
#endif
		
		gzDoc = gzopen(native_string.data(), "wb");
		if (!gzDoc) {
			qDebug() << "Can't open xml-file " << native_string << " for saving!";
			qDebug() << "Error " << errno << ": " << strerror(errno);
			return false;
		}
		
		auto raw_text = domDocToText();
		auto written = gzwrite(gzDoc, raw_text.begin(), raw_text.size());
		if (written != raw_text.size()) {
			qDebug() << "Unable to write xml-file " << outputXML << " to disc! (" << written << "!=" << raw_text.size() << ")";
			return false;
		}
		
		gzclose(gzDoc);
	}
	else {
		QFile file(outputXML);
		if ( ! file.open(QIODevice::WriteOnly | QIODevice::Truncate) ) {
			qDebug() << "Can't open xml-file " << outputXML << " for saving!";
			return false;
		}
		
		QTextStream ts( &file );
		ts.setCodec("UTF-8");
		ts << domDocToText();
		file.close();
	}
// 	qDebug() << "Saved to " << outputXML;
	return true;
}

//------------------------------------------------------------------------------

bool MorpheusXML::saveAsDialog()
{
	QString directory = "."; 
	if ( QSettings().contains("FileDialog/path") ) {
		directory = QSettings().value("FileDialog/path").toString();
	}
	if (!name.isEmpty()) directory += QString("/") + name;
	QString fileName = QFileDialog::getSaveFileName(nullptr, "Select xml-file to save configuration!", directory, "Morpheus Model (*.xml);;Compressed Morpheus Model (*.xml.gz)");
	if (fileName.isEmpty())
		return false;
	// pull the focus, such that all edit operations finish
//    qApp->activeWindow()->setFocus();
	if (!fileName.endsWith(".xml")) {
		fileName.append(".xml");
	}
	QString outputXML = fileName;
	QFile file(outputXML);
	if( !file.open(QIODevice::WriteOnly) )
	{
		return false;
	}
	else
	{
		QTextStream ts( &file );
		ts.setCodec("UTF-8");
		ts << domDocToText();
		file.close();
		this->path = QFileInfo(fileName).filePath();
		this->name = QFileInfo(fileName).fileName();
// 		qDebug() << "1. FileDialog/path = " << QFileInfo(fileName).dir().path();
// 		qDebug() << "2. Writing of XML-file completed!";
		QSettings().setValue("FileDialog/path", QFileInfo(fileName).dir().path());
		return true;
	}
}

//------------------------------------------------------------------------------

void MorpheusXML::DisabledToComments(QList<Replacement>& replacements, QDomElement element) const
{
	if (element.hasAttribute("internal:disabled") ) {
		qDebug() <<  element.nodeName() << "internal:disabled" << element.attributeNode("internal:disabled").nodeValue();
		 if (element.attributeNode("internal:disabled").nodeValue() == "true") {
			 // do the comment replacement
			auto parent = element.parentNode();
			// create the comment to replace the data node in the xml
			auto comment = element.ownerDocument().createComment("");
			auto disabledNode = element.ownerDocument().createElement("Disabled");
			// reparent the data node and store it into the comment
			disabledNode.appendChild(parent.replaceChild(comment, element));
			element.removeAttribute("internal:disabled");
			QString dis_node_text;
			QTextStream s(&dis_node_text);
			disabledNode.save(s,4);
			comment.setNodeValue(dis_node_text);
			element.setAttribute("internal:disabled","true");
			// store the replacement for rollback
			Replacement r {parent.toElement(), element, comment};
			replacements.append(r);
			return;
		 }
	}
	
	auto childs = element.childNodes();
	for (int i=0; i<childs.size(); i++ ) {
		if (childs.at(i).isElement())
			DisabledToComments(replacements, childs.at(i).toElement());
	}
}

QByteArray MorpheusXML::domDocToText() const {
	QList<Replacement> replacements;
	QDomElement root = (const_cast<MorpheusXML*>(this))->xmlDocument.documentElement();
	DisabledToComments(replacements, root);
	
	QByteArray data(xmlDocument.toByteArray(4));
	
	// revert comment replacements
	for ( auto& r : replacements) {
		r.parent.replaceChild(r.disabled_node, r.comment).clear();
	}
	return data;
}

//------------------------------------------------------------------------------

QString MorpheusXML::getNewModelName() {
    static int new_file_counter = 0;

    QString s = "untitled";
    if (new_file_counter > 0 )
        s .append(QString("_%1").arg(new_file_counter));

    new_file_counter++;
    return s;
}

