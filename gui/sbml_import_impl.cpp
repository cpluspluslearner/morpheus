#include "sbml_import_impl.h"
#include "sbml_converter.h"


SharedMorphModel SBMLImporter::importSBML(QString path) {
	QScopedPointer<SBMLImporter> importer(new SBMLImporter(nullptr, config::getModel()));
	importer->path->setText( path );
	int ret;
	do {
		ret = importer->exec();
		if (ret==QDialog::Accepted) {
			auto ret = importer->import();
			if (ret.first) {
				return ret.first;
			}
		} 
		else 
			break;
	} while (1);
	
	return nullptr;
}

SharedMorphModel SBMLImporter::importSBML(QByteArray data, bool global) {
	QScopedPointer<SBMLImporter> importer(new SBMLImporter(nullptr, config::getModel()));
	qDebug() << QString(data);
	importer->path->setEnabled(false);
	importer->path->setText("webdata ...");
	importer->path_dlg->setEnabled(false);
	int ret;
	do {
		ret = importer->exec();
		if (ret==QDialog::Accepted) {
			auto ret = importer->import(data);
			if (ret.first) {
				return ret.first;
			}
		} 
		else 
			break;
	} while (1);
	
	return nullptr;
}

SBMLImporter::SBMLImporter(QWidget* parent, SharedMorphModel current_model) : QDialog(parent)
{
	this->setMaximumWidth(500);
	this->setMinimumHeight(250);
	auto layout = new QVBoxLayout(this);
	auto grid_layout = new QGridLayout();
	this->setLayout(layout);

	setWindowTitle("SBML Import");

// 	layout->addSpacing(20);
	QGroupBox* frame = new QGroupBox("", this);

	frame->setLayout(new QHBoxLayout());
	frame->setFlat(true);

	QLabel* excl = new QLabel(this);
	excl->setPixmap(style()->standardIcon(QStyle::SP_MessageBoxWarning).pixmap(50,50));
	frame->layout()->addWidget(excl);

	QLabel* disclaimer = new QLabel(this);
	disclaimer->setWordWrap(true);
	disclaimer->setText(
		"Select an SBML model file to import.\n\n"
		"Note: Not all SBML concepts are supported. Units are discarded. Simulation details are set to default values.");

	frame->layout()->addWidget(disclaimer);
	layout->addWidget(frame);
	layout->addStretch(1);

	layout->addLayout(grid_layout);
	QLabel* path_label = new QLabel("SBML File ",this);
	grid_layout->addWidget(path_label,1,0,Qt::AlignRight);

	path = new QLineEdit(this);
	path_label->setBuddy(path);
	grid_layout->addWidget(path,1,1);
	
	path_dlg = new QPushButton(this);
	path_dlg->setIcon(style()->standardIcon(QStyle::SP_DirOpenIcon));
	connect(path_dlg,SIGNAL(clicked(bool)),this,SLOT(fileDialog()));
	grid_layout->addWidget(path_dlg,1,2);
	
	into_celltype  = new QComboBox(this);
	into_celltype->addItem("Global (new model)", "new,global");
	into_celltype->addItem("CellType (new model)", "new,celltype,sbml_cell");
	
	if (current_model) {
		into_celltype->insertSeparator(2);
		into_celltype->addItem("Global", "current,global");
		QString new_celltype = "sbml_cell";
		int i=0;
		for (const auto& part :current_model->parts ) {
			if (part.label=="CellTypes" && part.enabled) {
				part.element->getChilds();
				for (auto ct : part.element->activeChilds("CellType")) {
					auto ct_name = ct->attribute("name")->get();
					into_celltype->addItem(
						QString("CellType \"%1\"").arg(ct_name),
						QString("current,celltype,%1").arg(ct_name)
					);
					// Check name to not overlap new celltype naming convention
					if (ct_name == new_celltype && i==0)
						i=1;
					else {
						QRegExp ct_id("sbml_cell_(\\d+)");
						if (ct_id.exactMatch(ct_name)) {
							if (i<ct_id.cap(0).toInt())
								i = ct_id.cap(0).toInt();
						}
					}
				}
			}
		}
		if (i!=0)
			into_celltype->addItem("new CellType", QString("current,celltype,sbml_cell_%1").arg(i));
		else 
			into_celltype->addItem("new CellType", QString("current,celltype,sbml_cell"));
		model = current_model;
	}
	
	QLabel* celltype_label = new QLabel("Import into ",this);	
	celltype_label->setBuddy(into_celltype);
	grid_layout->addWidget(celltype_label,2,0,Qt::AlignRight);
	grid_layout->addWidget(into_celltype,2,1);

	tag = new QLineEdit(this);
// 	tag->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::MinimumExpanding);
	auto tag_label = new QLabel("Tag",this);
	grid_layout->addWidget(tag_label,3,0,Qt::AlignRight);
	grid_layout->addWidget(tag,3,1);

	
	QHBoxLayout *bottom = new QHBoxLayout();
	bottom->addStretch(1);
	
	auto ok = new QPushButton( "Import", this );
	connect( ok, SIGNAL(clicked()), SLOT(accept()) );
	bottom->layout()->addWidget(ok);

	auto cancel = new QPushButton( "Cancel", this );
	connect( cancel, SIGNAL(clicked()), SLOT(reject()) );
	bottom->addWidget(cancel);
	layout->addStretch(2);
	layout->addLayout(bottom);
}


void SBMLImporter::fileDialog()
{
	QString directory = ".";
	if ( QSettings().contains("FileDialog/path") ) {
		directory = QSettings().value("FileDialog/path").toString();
	}
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open SBML model"), directory, tr("SBML Files (*.xml)"));
	if (! fileName.isEmpty() ) {
		path->setText(fileName);
		QString path = QFileInfo(fileName).dir().path();
		QSettings().setValue("FileDialog/path", path);
	}
}


SBML2MorpheusML_Converter::ResultType SBMLImporter::import(QByteArray data) {
	SBML2MorpheusML_Converter::TargetDescr descr;
	auto target = into_celltype->itemData(into_celltype->currentIndex()).toString().split(",");
	
	if (target[0] != "new") {
		descr.model = model;
		model_created = false;
	}
	else
		model_created = true;
	
	if (target.size()>=2)
		descr.cell_type = target[1];
	
	descr.tag = tag->text();
	
	try {
		if (data.isEmpty()) {
			return SBML2MorpheusML_Converter::readSBML(path->text(), descr);
		}
		else {
			return SBML2MorpheusML_Converter::readSBML(data, descr);
		}
	}
	catch (SBMLConverterException e) {
		qDebug() << "Unable to import SBML due to " <<  QString::fromStdString(e.type2name())<< " "<< QString::fromStdString(e.what());
		QMessageBox::critical(this,"SBML Import Error", QString("Unable to import %1 due to the following error:\n%2 - %3").arg(path->text(),QString::fromStdString(e.type2name()), QString::fromStdString(e.what())), QMessageBox::Ok);
	}
	catch (QString s) {
		qDebug() << "Unable to import SBML due to " << s;
		QMessageBox::critical (this,"SBML Import Error", QString("Unable to import %1 due to the following error:\nSBML_INTERNAL_ERROR - %2").arg(path->text(),s),QMessageBox::Ok);
	}
	catch (...) {
		qDebug() << "Unable to import SBML due to unknown error";
		QMessageBox::critical (this,"SBML Import Error", QString("Unable to import %1 due to an unknown error:").arg(path->text()),QMessageBox::Ok);
	}
	
	return SBML2MorpheusML_Converter::ResultType() ;
}
