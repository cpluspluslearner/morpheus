#include "ad_solver.h"
#include "fwd_euler_ad_solver.h"
#include "implicit_ad_solver.h"
#include <xtensor/xio.hpp>
#include <xtensor/xnoalias.hpp>

std::shared_ptr<AD_Solver_I> AD_Solver_Base::createInstance(const AD_Descriptor& descr) {
	if (descr.ad_solver == AD_Solver::WellMixed)  {
		cout << "Creating WellMixedSolver"<< endl;
		return std::make_shared<WellMixedSolver>(descr);
	}
	else if (descr.ad_solver == AD_Solver::FwdEuler) {
		cout << "Creating FwdEulerADSolver"<< endl;
		return std::make_shared<FwdEulerADSolver>(descr);
	}
	else if (descr.ad_solver == AD_Solver::Implicit) {
		cout << "Creating ImplicitADSolver"<< endl;
		return std::make_shared<ImplicitADSolver>(descr);
	}
	return std::shared_ptr<AD_Solver_I>();
}

void AD_Solver_Base::setADBoundaries(AD_Descriptor::scalar_tensor *x0) const {
	const VINT s_origin = VINT(0,0,0); /// origin of the shadow region 
	const VINT i_origin = d.origin;
	const VINT shadow_width = d.origin;
	const VINT i_top = d.top;
	const VINT s_top = d.top + shadow_width ;           /// top of the shadow region
	auto& data = *x0;
	
	// for (int i=0; i<d.boundary_types.size(); i++) {
	// 	cout << "Boundary " << Boundary::code_to_name(Boundary::Codes(i)) << " type " << Boundary::type_to_name(d.boundary_types[i])  << " is" << (d.boundary_values[i]->isSpaceConst() ? "" : " not") << " space-constant has value " << d.boundary_values[i]->get(VINT()) << endl;
	// }
	
	// On hexagonal lattice there are 2 neihgbors at a boundary
	if (  false /*lattice.getStructure() == Lattice::hexagonal_new*/) 
	{}
	else {
		if (shadow_width.x != 1) {
			throw (string("AD_Solver_Base::setADBoundaries Missing implementation for shadown width " + to_str(shadow_width.x)));
		}
		
		VINT start = s_origin;
		VINT stop  = s_top+1;
		
		if (d.boundary_types[Boundary::mx] == Boundary::periodic) {
#pragma omp parallel for collapse(2)
			for (int z=start.z; z<stop.z; z++) {
				for (int y=start.y; y<stop.y; y++) {
						data(z,y,s_origin.x) = data(z,y,i_top.x);
						data(z,y,s_top.x) = data(z,y,1);
				}
			}
		}
		else {
			const bool xm_const = d.boundary_values[Boundary::mx]->isSpaceConst();
			const auto xm_value = xm_const ? d.boundary_values[Boundary::mx]->get(VINT()) : 0.0;
			const bool xp_const = d.boundary_values[Boundary::px]->isSpaceConst();
			const auto xp_value = xp_const ? d.boundary_values[Boundary::px]->get(VINT()) : 0.0;
#pragma omp parallel for collapse(2)
			for (int z=start.z; z<stop.z; z++) {
				for (int y=start.y; y<stop.y; y++) {
					data(z,y,s_origin.x) = xm_const ? xm_value : d.boundary_values[Boundary::mx]->get(VINT(s_origin.x,y,z)-shadow_width);
					data(z,y,s_top.x) = xp_const ? xp_value : d.boundary_values[Boundary::px]->get(VINT(s_top.x,y,z)-shadow_width);
				}
			}
		}
		
		
		if (shadow_width.y>0) {
			auto ymb = xt::view(data, xt::all(), s_origin.y, xt::all());
			auto ypb = xt::view(data, xt::all(), s_top.y,    xt::all());
			if (d.boundary_types[Boundary::my] == Boundary::periodic) {
				xt::noalias(ymb) = xt::view(data, xt::all(), i_top.y, xt::all());
				xt::noalias(ypb) = xt::view(data, xt::all(), i_origin.y, xt::all());
			}
			else {
				if (d.boundary_values[Boundary::my]->isSpaceConst()) {
					ymb.fill( d.boundary_values[Boundary::my]->get(VINT()) );
				}
				else {
					// manually iterate over VINT positions
					VINT start = i_origin;
					VINT stop  = i_top+1;
					start.y = s_origin.y;
					stop.y = i_origin.y;
					VINT pos = start;
					
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::my]->get(pos-shadow_width);
							}
				}
				if (d.boundary_values[Boundary::py]->isSpaceConst()) {
					ypb.fill( d.boundary_values[Boundary::py]->get(VINT()) );
				}
				else {
					// manually iterate over VINT positions
					VINT start = i_origin;
					VINT stop  = i_top+1;
					start.y = s_top.y;
					stop.y = s_top.y+1;
					VINT pos = start;
					
					for (; pos.z<stop.z; pos.z++) {
						for (pos.y=start.y; pos.y<stop.y; pos.y++) {
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::py]->get(pos-shadow_width);
							}
						}
					}
				}
			}
		}
		
		if (shadow_width.z>0) {
			auto zmb = xt::view(data, s_origin.z, xt::all(), xt::all());
			auto zpb = xt::view(data, s_top.z,    xt::all(), xt::all());
			if (d.boundary_types[Boundary::mz] == Boundary::periodic) {
				xt::noalias(zmb) = xt::view(data, i_top.z, xt::all(), xt::all());
				xt::noalias(zpb) = xt::view(data, i_origin.z, xt::all(), xt::all());
			}
			else {
				if (d.boundary_values[Boundary::mz]->isSpaceConst()) {
					zmb.fill( d.boundary_values[Boundary::mz]->get(VINT()) );
				}
				else {
					// manually iterate over VINT positions
					VINT start = i_origin;
					VINT stop  = i_top+1;
					start.z = s_origin.z;
					stop.z = i_origin.z;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::mz]->get(pos-shadow_width);
							}
				}
				if (d.boundary_values[Boundary::pz]->isSpaceConst()) {
					zpb.fill( d.boundary_values[Boundary::pz]->get(VINT()) );
				}
				else {
					// manually iterate over VINT positions
					VINT start = i_origin;
					VINT stop  = i_top+1;
					start.z = s_top.z;
					stop.z = s_top.z+1;
					VINT pos = start;
					for (; pos.z<stop.z; pos.z++)
						for (pos.y=start.y; pos.y<stop.y; pos.y++)
							for (pos.x=start.x; pos.x<stop.x; pos.x++) {
								data(pos.z,pos.y,pos.x) = d.boundary_values[Boundary::pz]->get(pos-shadow_width);
							}
				}
			}
		}
	}
}

