#include "domain.h"
#include "scope.h"
#include "simulation.h"


std::string Boundary::type_to_name(Boundary::Type t)
{
	switch (t) {
		case Boundary::noflux:
			return "noflux";
		case Boundary::flux:
			return "flux";
		case Boundary::value:
			return "value";
		case Boundary::periodic:
			return "periodic";
		default:
			return "none";
	}
}

Boundary::Type Boundary::name_to_type(string s)
{
	lower_case(s);
	if (s=="noflux")
		return noflux;
	else if (s=="flux") 
		return flux;
	else if (s=="constant")
		return constant;
	else if (s=="value")
		return value;
	else if (s=="periodic")
		return periodic;
	throw(string("Unknown boundary type ") + s);
}

std::string Boundary::code_to_name(Boundary::Codes code)
{
	switch (code) {
		case mx :
			return "-x";
		case px :
			return "x";
		case my : 
			return "-y";
		case py :
			return "y";
		case mz :
			return "-z";
		case pz :
			return "z";
		case domain :
			return "domain";
	}
	return "";
}


Boundary::Codes Boundary::name_to_code(std::string s)
{
	lower_case(s);
	if (s == "-x")
		return Boundary::mx; 
	else if (s == "-y")
		return Boundary::my;
	else if (s == "-z")
		return Boundary::mz;
	else if (s == "x")
		return Boundary::px; 
	else if (s == "y")
		return Boundary::py;
	else if (s == "z")
		return Boundary::pz;
	else if (s == "domain") {
		return Boundary::domain;
	}
	throw (string("Unknown boundary code ") + s);
	return Boundary::mx;
}

std::ostream& operator << (std::ostream& os, const Boundary::Type& a) { return (os << Boundary::type_to_name(a)); }
std::istream& operator >> (std::istream& is, Boundary::Type& a) { string s; is >> s; a = Boundary::name_to_type(s); return is; }

#include "lattice_data_layer.cpp"
#include "tiffio.h"
// explicit template instantiation ...
// template class Lattice_Data_Layer<Boundary::Type>;


void Domain::loadFromXML(const XMLNode xNode, Scope* scope, const LatticeDesc& lattice_desc)
{
	boundary_type = Boundary::flux;
	getXMLAttribute(xNode, "boundary-type", boundary_type);
	if (xNode.nChildNode("Image")) {
		type = Type::image;
		bool invert = false;
		getXMLAttribute(xNode, "Image/path", image_path);
		getXMLAttribute(xNode, "Image/invert", invert);

		createImageMap(image_path, invert);
		domain_size = image_size;
		if (lattice_desc.structure == Lattice::hexagonal || lattice_desc.structure == Lattice::hexagonal_new) {
			domain_size.y /= M_HALF_SQRT3;
		}
	}
	else if (xNode.nChildNode("Circle")) {
		type = Type::circle;
		getXMLAttribute(xNode, "Circle/diameter", diameter);
		domain_size = VINT(diameter+4,diameter+4,1);
		if (lattice_desc.structure == Lattice::hexagonal)
			domain_size.x*=sqrt(3);
		if (lattice_desc.structure == Lattice::hexagonal || lattice_desc.structure == Lattice::hexagonal_new) {
			domain_size.y /= M_HALF_SQRT3;
		}
	}
	else if (xNode.nChildNode("Hexagon")) {
		type = Type::hexagon;
		getXMLAttribute(xNode, "Hexagon/diameter", diameter);
		domain_size = VINT(diameter, sin(M_PI/3)*diameter, 1);
		if (lattice_desc.structure == Lattice::hexagonal)
			domain_size.x*=1.5;
		if (lattice_desc.structure == Lattice::hexagonal || lattice_desc.structure == Lattice::hexagonal_new) {
			domain_size.y /= M_HALF_SQRT3;
		}
	}
	else if (xNode.nChildNode("Expression")) {
		type = Type::expression;
		cout << "Creating mask expression" << endl;
		string str_expression;
		getXMLAttribute(xNode, "Expression/text", str_expression);
		mask_expression = make_unique<ExpressionEvaluator<double> >(str_expression, scope);
	}
	else {
		throw MorpheusException("Unknown Domain specification.", xNode);
	}
	cout << "Domain size " << domain_size << endl;
}

void Domain::init(Lattice* l) {
	lattice = l;
	if (type == Type::image) {
		VINT lattice_size = max(lattice->size(), domain_size);
		image_offset = max((lattice_size - image_size) /2, VINT(0,0,0));
	}
	else if (type == Type::circle) {
		VINT lattice_size = max(domain_size, lattice->size());
		center = lattice_size / 2;
		if (lattice->structure == Lattice::Structure::hexagonal && lattice->get_boundary_type(Boundary::mx) == Boundary::periodic) {
			center.x -= center.y/2;
		}
	}
	else if (type == Type::hexagon) {
		VINT lattice_size = max(domain_size, lattice->size());
		center = lattice_size / 2;
		if (lattice->structure == Lattice::Structure::hexagonal && lattice->get_boundary_type(Boundary::mx) == Boundary::periodic) {
			center.x -= center.y/2;
		}

	}
	else if (type == Type::expression) {
	}
	else {
		return;
	}
}

bool Domain::inside(const VINT& a) const
{
	switch (type) {
		case Domain::image :
			return insideImageDomain(a);
		case Domain::circle :
			return insideCircularDomain(a);
		case Domain::hexagon :
			return insideHexagonalDomain(a);
		case Domain::expression:
			return mask_expression->get(a)>0.0;
		default :
			return true;
	}
}

bool Domain::insideCircularDomain(const VINT& a) const
{
	VDOUBLE radius = lattice->orth_distance(lattice->to_orth(a),lattice->to_orth(center));
	radius.z=0;
	return (radius.abs() <= diameter/2);
}

bool Domain::insideHexagonalDomain(const VINT& a) const
{
	VDOUBLE r = lattice->orth_distance(lattice->to_orth(a),lattice->to_orth(center));
	r.z=0;
// 	auto size = domain_size/2;
	auto r_y = M_HALF_SQRT3 * diameter / 2.0;
	auto r_x = diameter / 2;
	if ( abs(r.y) <= r_y + 0.01 && (abs(r.x) <= (r_x-abs(r.y)/sin(M_PI/3)/2) + 0.01))
		return true;
	return false;
}

int Domain::image_index(const VINT& a) const
{
	return (a.x + a.y * image_size.x + a.z * image_size.x*image_size.y);
}


bool Domain::insideImageDomain(const VINT& a) const
{
	
	VDOUBLE dpos =  lattice->to_orth(a) - lattice->to_orth(image_offset);
	lattice->orth_resolve(dpos);
	VINT pos(round(dpos.x),round(dpos.y),dpos.z);
	if (pos.x<0 || pos.x>=image_size.x || pos.y<0 || pos.y>=image_size.y || pos.z<0 || pos.z>=image_size.z ) {
		return false;
	}
	return image_map[image_index(pos)];
}

void Domain::createEnumerationMap() const {
	cout << "Creating enumeration map" << endl;
	VINT pos;
	VINT size = lattice->size();
	// EnumRow row;
	// domain_enumerated_size = 0;
	for (pos.z=0; pos.z<size.z; pos.z++) {
		for (pos.y=0; pos.y<size.y; pos.y++) {
			for (pos.x=0; pos.x<size.x; pos.x++) {
				if (inside(pos)) {
					domain_enumeration.push_back(pos);
				}
			}
		}
	}
}


void Domain::createImageMap(string path, bool invert) {
	
	TIFFSetWarningHandler(0);
	TIFF* tif = TIFFOpen(path.c_str(), "r");
	if (! tif)
		throw(string("Unable to open Domain image ") + path + ".");
	
	cout << "Loading Domain Image" << endl;
	uint32_t width, height, slices;
	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
	slices = TIFFNumberOfDirectories(tif);
	
	image_size = VINT(width,height,slices);
	image_map.resize(width * height * slices);
	uint16_t bps, spp = 1;
	TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bps);
	TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &spp);

	
	bool broken_samples = false;
	if (spp==0 || spp>4) {
		broken_samples = true;
		spp=1;
	}
	cout << "Domain Tiff: " <<width<<"x"<<height<<","<<spp<<"x"<<bps<<"bit"<<endl;
	// always allocate 4 byte per pixel 8bit rgba / 32bit float
	size_t npixels_buffer = width;
	if (bps == 8) {
		npixels_buffer *= height;
	}
	auto  pbuffer = make_unique<uint32_t[]>(npixels_buffer);
	uint32_t* buffer = pbuffer.get();
	VINT pos;
	do {
		uint32_t w, h;
		uint16_t b,s=1;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
		TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &b);
		TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &s);
		
		if (broken_samples || s==0 || s>4) {
			s=1;
			broken_samples = true;
		}

		if (width!=w || height!=h || bps !=b || spp != s ){
			throw (string("imageDomain: Require identical sizes and color-resolution for images in a stack!"));
		}
		
		if( bps == 8){
//			if (TIFFReadRGBAImage(tif, w, h, raster, 0)) { // origin is bottom-left
			if (TIFFReadRGBAImageOriented(tif, w, h, buffer, ORIENTATION_TOPLEFT, 0)) { // ORIENTATION_BOTRIGHT, ORIENTATION_TOPRIGHT, ORIENTATION_TOPLEFT
				pos.x=0; pos.y=0;
				int image_offset = w*h*pos.z;
				for (uint i=0; i<npixels_buffer; i++) {
					uint8_t* channels = (uint8_t*) (buffer+i);
					uint32_t grey_val = uint32_t(channels[0]) + uint32_t(channels[1]) +uint32_t( channels[2]);
					
					if(!invert){
						image_map[i + image_offset] = (grey_val > 0);
					}
					else{
						image_map[i + image_offset] = (grey_val <= 0);
					}
				}
			}
		}
		else if( bps == 16 && spp==1){
			for (pos.y = 0; pos.y<h; pos.y++) {
				if (!TIFFReadScanline(tif, buffer, pos.y))
					throw string("Error while reading domain file ")+path;
				int image_offset = w*(h*pos.z + (h-pos.y-1));
				for (pos.x=0; pos.x<w; pos.x++) {
					image_map[pos.x + image_offset] = ((uint16_t*)buffer)[pos.x] >= 1;
				}
			}
		}
		else if (bps == 32 && spp==1) {
			for (pos.y = 0; pos.y<h; pos.y++) {
				if (!TIFFReadScanline(tif, buffer, pos.y))
					throw string("Error while reading domain file ")+path;
				int image_offset = w*(h*pos.z + (h-pos.y-1));
				for (pos.x=0; pos.x<w; pos.x++) {
					image_map[pos.x + image_offset] = ((float*)buffer)[pos.x] >= 1;
				}
			}
		}
		else {
			throw string("Domain: Unable to load ") + to_str(bps) + "bit Tiff image.";
		}
		pos.z++;
	} while( TIFFReadDirectory(tif) );
	
	TIFFClose(tif);
}
