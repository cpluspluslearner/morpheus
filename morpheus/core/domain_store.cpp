#include "domain_store.h"

void domain_store::insert(const VINT& n, bool ignore_duplicates) {
	// find the right starting patch
	auto pos = lower_bound(data.begin(), data.end(),n, 
		[](const StrideData& a, const VINT& b){ 
		return a.start.z<b.z || (a.start.z==b.z && a.start.y<b.y) || (a.start.z==b.z && a.start.y==b.y && a.start.x+a.length < b.x);
	});
	// empty
	bool insert_before = false;
	if (pos == data.end()) {
		insert_before = true;
	}
	// ? append
	else if (pos->start.z==n.z && pos->start.y == n.y)  {
		if (pos->start.x-1 == n.x) {
			pos->start.x-=1; pos->length++;
			_size++;
		}
		else if (pos->start.x+pos->length == n.x) {
			pos->length++;
			_size++;
			auto npos = pos;
			npos++;
			if (npos != data.end() && npos->start == n + VINT(1,0,0)) {
				pos->length += npos->length;
				data.erase(npos);
			}
		}
		else if (pos->start.x -1 > n.x) {
			insert_before = true;
		}
		else if (!ignore_duplicates) {
			stringstream dump;
			dump << " dump ";
			for (const auto& s : data) {
				dump << s.start << "+" << s.length << " | ";  
			}
			
			throw string("Domain store already contains ") + to_str(n) + dump.str();
		}
	}
	else {
		insert_before = true;
	}
	
	if (insert_before) {
		data.insert(pos,{n,1});
		_size++;
	}
}

void domain_store::push_back(const VINT& n) {
	if (data.empty()) { 
		data.push_back({n,1}); ++_size;
	}
	else { 
		auto& b =data.back(); VINT last = b.start; last.x+= b.length;
		if (n==last) { ++b.length; ++_size; }
		else if (less_VINT()(last,n)) { data.push_back({n,1}); ++_size; }
		else { insert(n); }
	}
};

bool domain_store::remove(const VINT& n) {
	auto pos = lower_bound(data.begin(), data.end(),n, [](const StrideData& a, const VINT& b){ 
		return a.start.z<b.z || (a.start.z==b.z && a.start.y<b.y) || (a.start.z==b.z && a.start.y== b.y && a.start.x+a.length<b.x) ;
	});
	if (pos == data.end() ) {
		// throw string("Position not in domain store ") + to_str(n);
		cout << string("Position not in domain store ") + to_str(n);
		return false;
	}
	// cout << "Remove " << n << " picked stride " << pos->start << "+" << pos->length << endl;
	if (!(pos->start.z==n.z && pos->start.y == n.y) || pos->start.x+pos->length<=n.x) {
		// throw string("Position not in domain store ") + to_str(n);
		cout << string("Position not in domain store ") + to_str(n);
		return false;
	}
	// ? front
	if (pos->start == n) {
		pos->start.x++; pos->length--;
		_size--;
	}
	// ? end
	else if (pos->start.x+pos->length-1 == n.x) {
		pos->length--;
		_size--;
	}
	// ? mid
	else {
		// split
		StrideData s1 {pos->start, n.x - pos->start.x };
		pos->start = n; pos->start.x+=1; pos->length -= s1.length +1;
		data.insert(pos,s1);
		_size--;
	}
	// sanitize 
	if (pos->length == 0 ) { data.erase(pos); }
	return true;
}


bool domain_store::erase(domain_store::iterator& pos) {
	if (pos.state.j < data.size() && pos.state.i < data[pos.state.j].length)  {
		if (pos.state.i == 0) {
			data[pos.state.j].start.x++; data[pos.state.j].length--; 
		}
		else if(pos.state.i+1 == data[pos.state.j].length) {
			data[pos.state.j].length--;
		}
		else {
			StrideData s1 = { data[pos.state.j].start, pos.state.i };
			data[pos.state.j].start.x+=pos.state.i+1;
			data[pos.state.j].length = data[pos.state.j].length - s1.length -1;
			data.insert(data.begin()+pos.state.j,s1);
		}
		--_size;
		return true;
	}
	else {
		return false;
	}
}

void domain_store::iterator::increment() {
	++state.i;
	if (state.i>=(*d)[state.j].length) {
		if (state.j+1>=d->size()) {
			state.i = (*d)[state.j].length;
			value = {0,0,0};
		}
		else {
			state.j++;
			state.i=0;
		}
		set_value();
	}
	else {
		++value.x;
	}
}
		
void domain_store::iterator::decrement() {
	if (state.i==0) {
		if (state.j>0) {
			--state.j;
			state.i=(*d)[state.j].length-1;
			set_value();
		}
	}
	else {
		--state.i;
		--value.x;
	}
}

domain_store::iterator::difference_type domain_store::iterator::distance_to(const iterator& z) const
{
	if (z.state.j==state.j) { return z.state.i-state.i; }
	else if (z.state.j>state.j) {
		difference_type dist=(*d)[state.j].length-state.i;
		auto j=state.j+1;
		while (j<z.state.j) { dist+=(*d)[j].length; ++j; }
		dist+=z.state.i;
		return -dist;
	}
	else {
		difference_type dist=(*d)[z.state.j].length-z.state.i;
		auto j=z.state.j+1;
		while (j<state.j) { dist+=(*d)[j].length; ++j; }
		dist+=state.i;
		return dist;
	}
}

void domain_store::iterator::advance(difference_type n) {
	if ( n >0 ) {
		while (n>0) {
			if (state.i+n>=(*d)[state.j].length) {
				n-=(*d)[state.j].length - state.i;
				// end check
				if (state.j+1>=d->size()) {
					state.i = (*d)[state.j].length;
					value = {0,0,0};
					return;
				}
				state.i=0;
				state.j++;
			}
			else {
				state.i+=n;
				n=0;
			}
		}
	}
	else {
		n=-n;
		while (n>0) {
			if (state.i<=n) {
				n-=state.i;
				// end check
				if (state.j==0) {
					state.i = 0;
					value = {0,0,0};
					return;
				}
				state.i=(*d)[--state.j].length;
			}
			else {
				state.i-=n;
				n=0;
			}
		}
	}
	set_value();
}

void domain_store::iterator::set_value() {
	value = (*d)[state.j].start;
	value.x += state.i;
}

domain_store::iterator::iterator(const deque<domain_store::StrideData>* data, int pos)
	: state({0,0}), d(data)
{
	if (d->empty()) {
		state = {0,0};
		value = {0,0,0};
	}
	else {
		if (pos==0)  {
			value = (*d)[0].start;
		}
		else if (pos>0) {
			advance(pos);
		}
		else {
			state.j = d->size()-1;
			state.i = (*d)[state.j].length;
			value = {0,0,0};
		}
	}
}

