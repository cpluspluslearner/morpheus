#include "threading.h"
#include <iostream>
#include "random_functions.h"

namespace TP {
	
unsigned int numthreads = 0;
std::vector<std::pair<std::thread::id,int>> team_nums;

thread_local unsigned int team_num = 0;
// #pragma omp threadprivate(team_num)

unsigned int teamSize() {
	return ( (omp_get_max_threads() > 0) ? omp_get_max_threads() : std::thread::hardware_concurrency() );
}

unsigned int teamNum()
{
	return team_num;
}

void setTeamNum(unsigned int num)
{
// 	std::cout << "Setting thread " << std::this_thread::get_id() << " to team id " << num << std::endl;
	team_num = num;
	initSeed();
}
	
// ParallelThreadPool& getThreadPool() {
// 	static ParallelThreadPool thread_pool;
// 	if (thread_pool.size()==0) {
// 		cout << "!!! Creating thread pool of size " << teamSize() << " !!!" << endl;
// 		thread_pool.setSize( teamSize() );
// 		auto team_num_setter = [](unsigned int team_id){ TP::setTeamNum(team_id); };
// 		for (unsigned int team_id =0; team_id<thread_pool.size(); team_id++ ) {
// 			thread_pool.submit(team_num_setter,team_id,team_id);
// 		}
// 	}
// 	return thread_pool;
// }
	
}
