#ifndef THREADING_H
#define THREADING_H

// #include "utils/parallel_thread_pool.h"
#include "config.h"
#include <thread>
		
#ifdef HAVE_OPENMP
    #include <omp.h>
#else
    inline int omp_get_thread_num()  { return 0;} 
    inline int omp_get_num_threads() { return 1;}
    inline int omp_get_max_threads() { return 1;}
    typedef int omp_lock_t;
#endif


namespace TP {
	unsigned int teamSize();
	
	// ParallelThreadPool& getThreadPool();
	
	unsigned int teamNum();
	
	void setTeamNum(unsigned int t);
}

#define CPM_OMP_LOCKS
#if defined(HAVE_OPENMP) && defined(CPM_OMP_LOCKS)
#include "utils/omp_guard.h"
namespace CPM {
	using mutex = omp_mutex;
	using guard = omp_guard;
}
#else 
#define CPM_STD_LOCKS
#include <mutex>
namespace CPM {
	using mutex = std::mutex;
	using guard = std::lock_guard<std::mutex>;
}
#endif

#endif
