#ifndef CSVREADER_H
#define CSVREADER_H

#include "core/interfaces.h"
#include "core/celltype.h"

/** \defgroup CSVReader
\ingroup ML_Population
\ingroup InitializerPlugins
\brief Initializes cell population from CSV file.

\section Description
CSVReader loads a cell configuration for a population from a CSV file. Cells are seeded with a single node, and will reach their target volume during simulation.
Format is as follows:
~~~~~~~~~~~~~~~~~~~~~~{.csv}
\<cell name\>,\<pos.x\>,\<pos.y\>{,\<pos.z\>}
...
~~~~~~~~~~~~~~~~~~~~~~
where the z-position defaults to 0. 
  - \b filename denotes the csv file
  - \b scaling allows to scale the configuration defined in the csv

\section Example
~~~~~~~~~~~~~~~~~~~~~{.xml}
<CSVReader filename="cellpop1.csv" scaling="2,2,2" />
~~~~~~~~~~~~~~~~~~~~~
*/

class CSVReader : public Population_Initializer
{
private:
	PluginParameter2<string, XMLValueReader, RequiredPolicy> filename;
    PluginParameter2<VDOUBLE, XMLEvaluator, DefaultValPolicy > scaling;

	CellType* cell_type;

	string readTextFile(string filename);
	
	CPM::CELL_ID empty_state;
	vector<CPM::CELL_ID> cells_created;

public:
	CSVReader();
	DECLARE_PLUGIN("CSVReader");

    vector<CPM::CELL_ID> run(CellType *ct) override;
};

#endif // CSVREADER_H
