#include "init_cell_objects.h"

REGISTER_PLUGIN(InitCellObjects);

// angle contains rotation angles encoded phi, theta, rho, defining rotation along the z,y and x axis, executed in reverse order
// VDOUBLE rotate(VDOUBLE p, const VDOUBLE& angle) {
// 	double phi=angle.x, theta=angle.y, rho=angle.z;
// 	if (rho!=0) {
// 		p = VDOUBLE(
// 			p.x,
// 			p.y*cos(rho) - p.z*sin(rho),
// 			p.y*sin(rho) + p.z*cos(rho)
// 		);
// 	}
// 	if (theta!=0) {
// 		p = VDOUBLE(
// 			p.x*cos(theta) + p.z*sin(theta),
// 			p.y,
// 			-p.x*sin(theta) + p.z*cos(theta)
// 		);
// 	}
// 	return VDOUBLE(
// 		p.x*cos(-phi) - p.y*sin(-phi),
// 		p.x*sin(-phi) + p.y*cos(-phi)*cos(rho) - p.z*sin(rho),
// 		p.y*sin(rho) + p.z*cos(rho)
// 	);
// }

VDOUBLE InitCellObjects::CellObject::rotate(VDOUBLE p) const {
	if (theta!=0) {
		p = { p.x*cos_theta + p.z*sin_theta,
			  p.y,
			  -p.x*sin_theta + p.z*cos_theta };
	}
	return { p.x*cos_phi + p.y*sin_phi,
			 -p.x*sin_phi + p.y*cos_phi,
			 p.z};
}


void InitCellObjects::CellObject::setRotation(double p, double t) {
	phi = p; sin_phi = sin(phi); cos_phi=cos(phi);
	theta = t; sin_theta = sin(theta); cos_theta=cos(theta);
}


class Point : public InitCellObjects::CellObject {
public:
	Point(XMLNode node, const Scope * scope) : displacement(0,0,0)  { 
		p_center->setXMLPath("center");
		p_center->loadFromXML(node, scope);
	};
	Point(const Point&) = default;
	void init() override { 
		SymbolFocus cell(cellID());
		p_center->init(); _center = p_center->safe_get(cell) + displacement;
	}
	string name() const override  { return "Point"; }
	VDOUBLE center() const override { return _center; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Point>(*this); c->cell_id = id;  return c;}
	bool inside(const VDOUBLE& pos) const override { return distance(pos)<=0.5;}
	double affinity(const VDOUBLE & pos) const override { return inside( pos); } 
	double distance(const VDOUBLE& pos) const { return SIM::lattice().orth_distance(pos,_center).abs(); } ;
	void displace(VDOUBLE distance) override { displacement +=distance; }
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_center;
	VDOUBLE displacement;
	VDOUBLE _center;
};

class Sphere : public InitCellObjects::CellObject {
public:
	Sphere(XMLNode node, const Scope * scope) : displacement(0,0,0)  { 
		p_center->setXMLPath("center"); p_center->loadFromXML(node, scope);
		p_radius->setXMLPath("radius"); p_radius->loadFromXML(node, scope);
	};
	Sphere(const Sphere&) = default;
	void init() override {
		SymbolFocus cell(cellID());
		p_center->init(); 
		_center = p_center->safe_get(cell) + displacement;
		cell.setCell(cellID(), SIM::lattice().from_orth(_center));
		p_radius->init();
		radius = p_radius->safe_get( cell );
		sqr_radius = radius * radius;
		// boundary_touch = !(_center.x-radius>0 &&_center.x+radius < 
	}
	string name() const override  { return "Sphere"; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Sphere>(*this); c->cell_id = id;  return c;}
	VDOUBLE center() const override { return _center; }
	bool inside(const VDOUBLE& pos) const override { return SIM::lattice().orth_distance(pos,_center).abs_sqr() <= sqr_radius;}
	double affinity(const VDOUBLE & pos) const override {
		double dist = SIM::lattice().orth_distance(pos,_center).abs();
		return (dist<radius) ?  1 - dist/radius : 0;
	}
	double distance(const VDOUBLE& pos) const { 
		double dist = SIM::lattice().orth_distance(pos,_center).abs();
		if (dist<radius)
			return 0;
		return dist - radius; 
	};
	void displace(VDOUBLE distance) override { displacement +=distance; }
	
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_center;
	PluginParameter_Shared<double, XMLEvaluator, RequiredPolicy> p_radius;
	VDOUBLE displacement;
	VDOUBLE _center;
	double radius;
	double sqr_radius;
	bool boundary_touch;
};


class Box : public InitCellObjects::CellObject {
public:
	Box(XMLNode node, const Scope * scope) : displacement(0,0,0)  { 
		p_origin->setXMLPath("origin"); p_origin->loadFromXML(node, scope);
		p_size->setXMLPath("size"); p_size->loadFromXML(node, scope);
		p_rotation->setXMLPath("rotation"); p_rotation->setDefault("0.0, 0.0, 0.0"); p_rotation->loadFromXML(node, scope);
	};
	Box(const Box&) = default;
	void init() override { 
		SymbolFocus cell(cellID());
		p_origin->init();
		origin = p_origin->safe_get(SymbolFocus::global) + displacement-0.5;
		cell.setCell(cellID(), SIM::lattice().from_orth(origin));
		cell = SymbolFocus(cellID(),  SIM::lattice().from_orth(origin));
		p_size->init();
		size = p_size->safe_get( cell );
		p_rotation->init();
		rotation = p_rotation->safe_get( cell );
		setRotation(rotation.x,rotation.y);
		if (SIM::lattice().getDimensions()<=2)  size.z=1;
		if (SIM::lattice().getDimensions()<=1)  size.y=1;
		top = origin+size;
	}
	string name() const override  { return "Box"; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Box>(*this); c->cell_id = id;  return c;}
	VDOUBLE center() const override { return origin+0.5*size; }
	bool inside(const VDOUBLE& pos) const override {
		VDOUBLE rpos = center() + rotate(pos-center());
		return rpos.x >= origin.x && rpos.y >= origin.y && rpos.z >= origin.z &&
		rpos.x <= top.x && rpos.y <= top.y && rpos.z <= top.z;
	}
	
	double affinity(const VDOUBLE& pos) const override {
		double d = distance(pos);
		return (d<0) ? -d : 0;
	}
	double distance(const VDOUBLE& real_pos) const {
		VDOUBLE out_distance(0,0,0);
		VDOUBLE in_distance(0,0,0);
		bool inside = false;
		
		VDOUBLE rpos = center() + rotate(real_pos-center());
		auto pos = SIM::lattice().orth_distance(rpos, center()) + 0.5*size;
		
		if (pos.x<0)
			out_distance.x=-pos.x;
		else if (pos.x>=size.x) 
			out_distance.x = pos.x-size.x;
		else {
			in_distance.x = min(pos.x, size.x-pos.x);
		}
		
		if (pos.y<0)
			out_distance.y=-pos.y;
		else if (pos.y>=size.y)
			out_distance.y = pos.y-size.y;
		else {
			in_distance.y = min(pos.y, size.y-pos.y);
		}

		if (pos.z<0)
			out_distance.z=-pos.z;
		else if (pos.z>=size.z)
			out_distance.z = pos.z-size.z;
		else {
			in_distance.z = min(pos.z, size.z-pos.z);
		}

		inside = (out_distance.abs_sqr() == 0);
		if (inside) {
			// Distance from the boundary
			VDOUBLE dist = (2*in_distance)/size;
			if (SIM::lattice().getDimensions()==2) 
				return -sqrt(dist.x * dist.y);
			else if (SIM::lattice().getDimensions()==1) 
				return -dist.x;
			else 
				return -sqrt(dist.x * dist.y * dist.z);
		}
		else {
			return out_distance.abs();
		}
		
	} ;
	void displace(VDOUBLE distance) override { displacement +=distance; }
	
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_origin;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_size;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, DefaultValPolicy> p_rotation;
	VDOUBLE displacement;
	VDOUBLE origin;
	VDOUBLE rotation;
	VDOUBLE top;
	VDOUBLE size;
	
};

class Ellipsoid : public InitCellObjects::CellObject {
public:
	Ellipsoid(XMLNode node, const Scope * scope) : displacement(0,0,0)  { 
		p_center->setXMLPath("center"); p_center->loadFromXML(node, scope);
		p_axes->setXMLPath("axes"); p_axes->loadFromXML(node, scope);
		p_rotation->setXMLPath("rotation"); p_axes->loadFromXML(node, scope);
	};
	Ellipsoid(const Ellipsoid&) = default;
	void init() override { 
		SymbolFocus cell(cellID());
		p_center->init();
		_center = p_center->safe_get(cell) + displacement;
		cell.setCell(cellID(), SIM::lattice().from_orth(_center));
		p_axes->init();
		axes = p_axes->safe_get( cell );
		setFoci();
		p_rotation->init();
		rotation = p_rotation->safe_get( cell );
		setRotation(rotation.x,rotation.y);
	}
	string name() const override  { return "Ellipsoid"; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Ellipsoid>(*this); c->cell_id = id;  return c;}
	VDOUBLE center() const override { return _center; }
	/// distance from point to center, respecting periodic boundary conditions
	bool inside(const VDOUBLE& pos) const override {
		return distance(pos) <= 0;
		// auto d = SIM::lattice().orth_distance( _center,  pos);
		// double p  = (sqr(d.x))/sqr(axes.x) + (sqr(d.y))/sqr(axes.y) + (axes.z>0 && abs(d.z)>0 ? (sqr(d.z))/sqr(axes.z): 0.0);
		// return (p < 1);
	}
	
	double affinity(const VDOUBLE & pos) const override {
		auto d = distance(pos);
		return d<=0 ? -d : 0;
	}
	
	double distance(const VDOUBLE& real_pos) const {
		VDOUBLE rpos = center() + rotate(real_pos-center());
		auto d = SIM::lattice().orth_distance( _center,  rpos);
		double p  = (sqr(d.x))/sqr(axes.x) + (sqr(d.y))/sqr(axes.y) + (axes.z>0 && abs(d.z)>0 ? (sqr(d.z))/sqr(axes.z): 0.0);
		if (p<=1) return p-1;
		else 
			return (p-1) * max(axes.x,axes.y); // TODO This is samewhat arbitrary but might work nicely.
	} ;
	void displace(VDOUBLE distance) override { displacement +=distance; }
	
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_center;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_axes;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, DefaultValPolicy> p_rotation;
	VDOUBLE displacement;
	VDOUBLE _center;
	VDOUBLE axes;
	VDOUBLE focus1,focus2;
	VDOUBLE rotation;
	
	
	void setFoci() {
		// sort axes lengths from large to small
		//  we keep track of the indices
		vector< std::pair<double, int> > axis_length(3);
		axis_length[0] = std::pair<double, int>(axes.x, 0);
		axis_length[1] = std::pair<double, int>(axes.y, 1);
		axis_length[2] = std::pair<double, int>(axes.z, 2);
		std::sort(axis_length.begin(), axis_length.end(), [](const pair<double, int>& i, const pair<double, int>& j) {
			return (i.first > j.first); 
		}); // axes from large to small

		// a = semimajor axis length
		double a = axis_length[0].first;
		// b = semiminor axis length
		double b = axis_length[1].first;
		// distance of focus from center
		double focus = sqrt( sqr(a) - sqr(b) );

		// from the index, we can determine the x,y,z axis along which the focus oriented
		VDOUBLE df(0.,0.,0.);
		if( axis_length[0].second == 0 ){ // x is semimajor
			df.x = focus;
		}
		else if( axis_length[0].second == 1 ){ // y is semimajor
			df.y = focus;
		}
		else if( axis_length[0].second == 2 ){ // z is semimajor
			df.z = focus;
		}
		focus1 = _center + df;
		focus2 = _center - df;
	}
};

class Cylinder : public InitCellObjects::CellObject {
public:
	enum class Orientation  { X, Y, Z };

	Cylinder(XMLNode node, const Scope * scope) : displacement(0,0,0)  {
		p_origin->setXMLPath("origin"); p_origin->loadFromXML(node, scope);
		p_length->setXMLPath("length"); p_length->loadFromXML(node, scope);
		p_radius->setXMLPath("radius"); p_radius->loadFromXML(node, scope);
	};
	Cylinder(const Cylinder&) = default;
	void init() override {
		SymbolFocus cell(cellID()); 
		p_origin->init(); 
		origin = p_origin(cell) + displacement;
		cell.setCell(cellID(), SIM::lattice().from_orth(origin));
		p_length->init();
		p_radius->init();
		length = p_length(cell);
		radius = p_radius(cell);
	}
	string name() const override  { return "Cylinder"; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Cylinder>(*this); c->cell_id = id; return c;}
	VDOUBLE center() const override { 
		return origin + 0.5 * length;
	}
	bool inside(const VDOUBLE& pos) const override {
		auto dp = SIM::lattice().orth_distance( pos, center());
		auto d = cross(dp,length).abs() / length.abs();
		double t = dot(dp,length) / length.abs_sqr();
		return d<=radius && t>=-0.5 && t<=0.5;
	}
	double affinity(const VDOUBLE& pos) const override {
		auto dp = SIM::lattice().orth_distance( pos, center());
		auto d = cross(dp,length).abs() / length.abs();
		double t = dot(dp,length)/length.abs_sqr();
		if (d>radius || t<-0.5 || t>0.5) return 0.0;
		return 1.0-d/radius;
	};
	void displace(VDOUBLE distance) override { displacement +=distance; }
	
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_origin ;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_length;
	PluginParameter_Shared<double, XMLEvaluator, RequiredPolicy> p_radius;
	// actual values of the cylinder instance
	VDOUBLE origin, length;
	double radius;
	VDOUBLE displacement;
};

class Hexagon : public InitCellObjects::CellObject {
public:
	enum class Orientation  { X, Y, Z };

	Hexagon(XMLNode node, const Scope * scope) : displacement(0,0,0)  {
		p_center->setXMLPath("center"); p_center->loadFromXML(node, scope);
		p_height->setXMLPath("height"); p_height->loadFromXML(node, scope);
		p_radius->setXMLPath("radius"); p_radius->loadFromXML(node, scope);
		p_rotation->setXMLPath("rotation"); p_rotation->setDefault("0,0,0"); p_rotation->loadFromXML(node, scope);
	};
	Hexagon(const Hexagon&) = default;
	void init() override {
		SymbolFocus cell(cellID()); 
		p_center->init(); 
		_center = p_center(cell) + displacement;
		if (SIM::lattice().getDimensions()<3) _center.z =0;
		cell.setCell(cellID(), SIM::lattice().from_orth(_center));
		p_height->init();
		p_radius->init();
		height = p_height(cell);
		if (SIM::lattice().getDimensions()<3) height =0;
		radius = p_radius(cell);
		p_rotation->init();
		rotation = p_rotation->safe_get( cell );
		setRotation(rotation.x,0);
	}
	string name() const override  { return "Hexagon"; }
	unique_ptr<CellObject> clone(CPM::CELL_ID id) const override { auto c = make_unique<Hexagon>(*this); c->cell_id = id; return c;}
	VDOUBLE center() const override { return _center; }
	
	
	double distance(const VDOUBLE& pos) const {
		double phi_rotation=0;
		VDOUBLE dp;
		if (rotation.y==0) {
			phi_rotation = rotation.x;
			dp = SIM::lattice().orth_distance(pos,center());
		} 
		else 
			dp = rotate(SIM::lattice().orth_distance(pos,center()));
		
		auto dr = sqrt(dp.x*dp.x + dp.y*dp.y);
		// early jump offs
		if (dp.z > height/2 )
			return max(dp.z-height/2, dr-radius);
		else if (dr>radius)
			return dr-radius;
		
		auto dphi = fmod(dp.angle_xy() + M_PI/6 + phi_rotation, M_PI / 3) + M_PI/3;
		dp = VDOUBLE::from_radial(VDOUBLE(dphi,0,dr) );
		auto ri = radius*sqrt(3)/2;
		if (dp.y>ri)
			return dp.y - ri;
		else 
			return dp.y/ri-1;
	}
	
	bool inside(const VDOUBLE& pos) const override {
		return distance(pos) <=0; 
	}

	double affinity(const VDOUBLE& pos) const override {
		auto d = distance(pos);
		if (d<=0) {
			return -d;
		}
		return 0.0;
	};
	void displace(VDOUBLE distance) override { displacement +=distance; }
	
private:
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, RequiredPolicy> p_center;
	PluginParameter_Shared<double, XMLEvaluator, RequiredPolicy> p_height;
	PluginParameter_Shared<double, XMLEvaluator, RequiredPolicy> p_radius;
	PluginParameter_Shared<VDOUBLE, XMLEvaluator, DefaultValPolicy> p_rotation;
	// actual values of the cylinder instance
	VDOUBLE _center, rotation;
	double radius, height;
	VDOUBLE displacement;
};


InitCellObjects::InitCellObjects() : Population_Initializer() {
	mode.setXMLPath("mode");
	map<string, Mode> conv_map;
	conv_map["order"] = Mode::ORDER;
	conv_map["distance"] = Mode::DISTANCE;
	mode.setConversionMap(conv_map);
	mode.setDefault("distance");
	registerPluginParameter(mode);
};

void InitCellObjects::loadFromXML(const XMLNode node, Scope* scope)
{
	Population_Initializer::loadFromXML( node, scope );
}

//============================================================================

vector<CPM::CELL_ID> InitCellObjects::run(CellType* ct)
{
	local_scope = ct->getScope();
	
	PluginParameter2<VDOUBLE,XMLEvaluator,RequiredPolicy>  arrange_displacement;
	arrange_displacement.setXMLPath("displacements");
	PluginParameter2<VDOUBLE,XMLEvaluator,RequiredPolicy>  arrange_repetitions;
	arrange_repetitions.setXMLPath("repetitions");
	PluginParameter2<double,XMLEvaluator,DefaultValPolicy>  arrange_random_displacement;
	arrange_random_displacement.setXMLPath("random_displacement");
	arrange_random_displacement.setDefault("0");
	
	for (int i=0; i < stored_node.nChildNode("Arrangement"); i++) {
		XMLNode aNode = stored_node.getChildNode("Arrangement",i);
		arrange_repetitions.loadFromXML(aNode, local_scope);
		arrange_repetitions.init();
		arrange_displacement.loadFromXML(aNode, local_scope);
		arrange_displacement.init();
		arrange_random_displacement.loadFromXML(aNode, local_scope);
		arrange_random_displacement.init();
		
		VINT repetitions = VINT(arrange_repetitions(SymbolFocus::global));
		repetitions.x = (repetitions.x==0 ? 1 : repetitions.x);
		repetitions.y = (repetitions.y==0 ? 1 : repetitions.y);
		repetitions.z = (repetitions.z==0 ? 1 : repetitions.z);
		VDOUBLE displacement=arrange_displacement(SymbolFocus::global);
		double random_displacement = arrange_random_displacement(SymbolFocus::global);
		
 		cout << "InitCellObjects: Arrange: displacement: " << displacement << ", repetitions: " << repetitions << endl;

		unique_ptr<CellObject> c_blueprint;
		
		for (int j=0; j < aNode.nChildNode(); j++) {
			XMLNode oNode = aNode.getChildNode(j);
			string tag_name(oNode.getName());
			
			if (tag_name == "Point") 
				c_blueprint = make_unique<Point>(oNode, local_scope);
			else if (tag_name == "Sphere")
				c_blueprint = make_unique<Sphere>(oNode, local_scope);
			else if (tag_name == "Ellipsoid")
				c_blueprint = make_unique<Ellipsoid>(oNode, local_scope);
			else if (tag_name == "Box")
				c_blueprint = make_unique<Box>(oNode, local_scope);
			else if (tag_name == "Cylinder")
				c_blueprint = make_unique<Cylinder>(oNode, local_scope);
			else if (tag_name == "Hexagon")
				c_blueprint = make_unique<Hexagon>(oNode, local_scope);
			else
				throw MorpheusException(string("Unknown Object type ") + tag_name, oNode);
			
			uint num_before = cellobjects.size();
			arrangeObjectCombinatorial(std::move(c_blueprint), cellobjects, displacement, repetitions, random_displacement);
			uint num_after = cellobjects.size();
 			cout << "InitCellObjects: Arranged " << (num_after-num_before) << " objects." << endl;
		}
	}
	
// 	for( auto& obj : cellobjects ){
// 		
// // 		cout << "CellObject " << n << " = center: " << cellobjects[n]->center() << ", id " << cellobjects[n]->cellID() << endl;
// 	}
	
	int i = setNodes(ct);
	int unset =0;
	vector<CPM::CELL_ID> cells;
	for(int n = 0; n < cellobjects.size() ; n++){
		uint cellsize = ct->getCell(cellobjects[n]->cellID()).getNodes().size();
// 		cout << "CellObject " << n << " (" << ct->getName() << "), size = " << cellsize << endl;

		if( cellsize == 0 ){
			cout << "WARNING in InitCellObject: Cell " << n << " has zero size (perhaps due to overlap with other object?). Removing cell " << n << "." << endl;
			ct->removeCell( cellobjects[n]->cellID() );
			unset++;
		}
		else  {
			cells.push_back(cellobjects[n]->cellID());
		}
	}
	cout << "InitCellObject: Added " <<  cellobjects.size() - unset << " cell(s), occupying " << i << " nodes" << endl;
	return cells;
}

//============================================================================

void InitCellObjects::arrangeObjectCombinatorial( unique_ptr<CellObject> c_template, vector< unique_ptr<CellObject> >& objectlist, VDOUBLE displacement, VINT repetitions, double random_displacement )
{
	auto celltype = local_scope->getCellType();
	assert(celltype);
	VINT r(0,0,0);
	for(r.z=0; r.z<repetitions.z; r.z++){
		for(r.y=0; r.y<repetitions.y; r.y++){
			for(r.x=0; r.x<repetitions.x; r.x++){
				
				auto n = c_template->clone(celltype->createCell());
				
				n->displace(r*displacement);
				if(random_displacement > 0){
					n->displace( VDOUBLE(
					            ( getRandom01()-0.5) * random_displacement ,
					            ( SIM::lattice().getDimensions() > 1 ? (getRandom01()-0.5) * random_displacement : 0 ), 
					            ( SIM::lattice().getDimensions() > 2 ? (getRandom01()-0.5) * random_displacement : 0 )
					));
				}
				n->init();
				objectlist.emplace_back( std::move(n) );
			}
		}
	}
}

//============================================================================

int InitCellObjects::setNodes(CellType* ct)
{
	shared_ptr<const Lattice> lattice = SIM::getLattice();

	VINT pos;
	uint i=0;
	VINT lsize = lattice->size();
	
	for(pos.x = 0; pos.x < lsize.x ; pos.x++){
		for(pos.y = 0; pos.y < lsize.y ; pos.y++){
			for(pos.z = 0; pos.z < lsize.z ; pos.z++){

				// check whether multiple objects claim this lattice point
				vector<Candidate> candidates;
				VDOUBLE orth_pos = lattice->to_orth(pos);
				
				for(int o = 0; o < cellobjects.size() ; o++){
					if (cellobjects[o]->inside(orth_pos)) {
					// auto affinity = cellobjects[o]->affinity(orth_pos);
					// if ( affinity > 0 ) {
						candidates.push_back( {o, cellobjects[o]->affinity(orth_pos)} );
					}
				}

				// if multiple nodes, let first one (ORDER) or closest one (DISTANCE) have the nodes
				if( candidates.size() > 0 ){
					
					int winner_candidate=-1;

					switch( mode() ){
						case( Mode::ORDER ): // first one wins
						{  
							winner_candidate=0;
							break;
						}
						case( Mode::DISTANCE ): // smallest distance wins
						{ 
							double max_affinity = 0;
							for(int c=0; c<candidates.size();c++){
								if( candidates[c].affinity > max_affinity ){
									max_affinity = candidates[c].affinity;
									winner_candidate = c;
								}
							}
							break;
						}
						
					}
					
					if (winner_candidate==-1) winner_candidate=0;
					int winner_object_id = candidates[winner_candidate].index;
					
					if( CPM::getNode(pos) == CPM::getEmptyState() ) { // do not overwrite cells (unless medium)
						// take care that node positions in cells are contiguous, also in case of periodic boundary conditions
						VINT latt_center = lattice->from_orth(cellobjects[ winner_object_id ]->center());
						VINT pos_optimal = latt_center - lattice->node_distance( latt_center,  pos);
						CPM::setNode(pos_optimal, cellobjects[ winner_object_id ]->cellID() );
					}
				i++;
				}
			}
		}
	}
	return i;
}


// VDOUBLE InitCellObjects::distanceToLineSegment(VDOUBLE p, VDOUBLE l1, VDOUBLE l2)
// {
// 	// the line segment is given by 
// 	// l1 + t*(l2-l1), restricted to  0<=t<=1
// 	
// 	VDOUBLE p_l1  = lattice->orth_distance( lattice->to_orth(p), lattice->to_orth(l1) );
// 	VDOUBLE l2_l1 = lattice->orth_distance( lattice->to_orth(l2),lattice->to_orth(l1) );
// 
// 	double len_sq = dot(l2_l1,l2_l1); 
// 	double param = -1;
// 	if(len_sq != 0)
// 		param = dot(p_l1,l2_l1) / len_sq;
// 
// 	VDOUBLE line_pos;;
// 	if( param < 0 ){
// 		line_pos = l1;
// 	}
// 	else if( param > 1 ){
// 		line_pos = l2;
// 	}
// 	else{
// 		line_pos = l1 + param*l2_l1;
// 	}
// 	VDOUBLE dist = lattice->orth_distance( p, line_pos );
// 	return dist;
// }

// double min_elip_dist(VDOUBLE y, VDOUBLE elips) {
// 	double epst = 1e-3;
// 	double x0=0, x1=0.1;
// 	
// 	while (abs(x1-x0) > epst) {
// 		x0=x1;
// 		x1=x0-
// 	}
// }

// function [z,dist, lambda] = min_elp_dist(y,vec)
// epst = 1e-10;
// x0=0;
// x1=0.1;
// while abs(x1-x0)>epst
// x0 = x1;
// x1 = x0-fun(x0,y,vec)/der(x0,y,vec);
// end
// z = (eye(length(vec))+lam*diag(vec))\y;
// dist= norm(y-z);
//  
// function res = fun(lam,y,vec);
// res = sum(vec.*(y./(lam*vec+1)).^2)-1;
//  
// function res2 = der(lam,y,vec);
// res2 = -2*sum((vec.^2).*(y.^2)./((lam*vec+1).^3));
