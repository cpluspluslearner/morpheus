#include "interface_constraint.h"
#include "celltype.h"

REGISTER_PLUGIN(InterfaceConstraint);

InterfaceConstraint::InterfaceConstraint(){
	target.setXMLPath("target");
	strength.setXMLPath("strength");
	otherCellType.setXMLPath("type");
	
	registerPluginParameter(target);
	registerPluginParameter(strength);
	registerPluginParameter(otherCellType);
};

double InterfaceConstraint::delta ( const SymbolFocus& cell_focus, const CPM::Update& update ) const
{
	
	double old_length =0;
	double new_length =0;
	auto oct = otherCellType()->getID();
	for (const auto& face : cell_focus.cell().currentShape().interfaces()) {
		old_length += CellType::storage.index(face.first).celltype == oct ? face.second : 0.0;
	}
	for (const auto& face : cell_focus.cell().updatedShape().interfaces()) {
		new_length += CellType::storage.index(face.first).celltype == oct ? face.second : 0.0;
	}

	double target_interface_length = target(cell_focus);
	double s = strength( cell_focus );
	double dE = s * ( sqr(new_length - target_interface_length ) - sqr(old_length - target_interface_length ) );
	
	return dE;
}

double InterfaceConstraint::hamiltonian ( CPM::CELL_ID cell_id ) const
{
	SymbolFocus focus(cell_id);
	double interface_length =0;
	auto oct = otherCellType()->getID();
	for (const auto& face : focus.cell().currentShape().interfaces()) {
		interface_length += CellType::storage.index(face.first).celltype == oct ? face.second : 0.0;
	}
	double t = target( focus );
	return strength(focus) * sqr( interface_length - t);
}
