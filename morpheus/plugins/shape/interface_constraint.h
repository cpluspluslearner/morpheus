
//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef SURFACECONSTRAINT_H
#define SURFACECONSTRAINT_H

#include "core/interfaces.h"
#include "core/celltype.h"

/** \defgroup InterfaceConstraint
\ingroup ML_CellType
\ingroup CellShapePlugins
\brief Penalizes deviations from target cell interface length (2D) or interface area (3D) with cells of another cell type

\ingroup CPM_EnergyPlugins

The interface constraint penalizes deviations of the cell interface length (2D) or interface area \f$ s_{\sigma,\tau, t}\f$ with cells of another cell type \f$\tau\f$ from a given target \f$ S_{\tau} \f$.

This models a cell interface length conservation mechanism.

The Hamiltonian for cell \f$j\f$ is given by \f$ E_{\text{interface},j} = \lambda_\tau \cdot  ( \sum_{i} s(\sigma_j, \sigma_{\tau,i}) - S_{\tau} )^2 \f$

where 
- \f$ \sigma \f$ is the id of a cell.
- \f$ s(\sigma_1,\sigma_2) \f$ interface length of cells \f$ \sigma_1\f$ and \f$\sigma_2\f$.
- \f$ S_{\tau} \f$ is the target interface size of cell \f$ \sigma_j \f$ with cells of type \f$ \tau \f$.
- \f$ \lambda_\tau \f$ is strength of the constraint.

\section Input 
Required
--------
- *type*: Sum of the interface lengths with cells of this cell type will be constrained.

- *target*: Target interace length (2D) or interface area (3D) of a cell.

- *strength*: Strength of the interface constraint.
*/

class InterfaceConstraint : public CPM_Energy
{
private:

	PluginParameterCellType<RequiredPolicy> otherCellType;
	PluginParameter2<double, XMLThreadsafeEvaluator, RequiredPolicy> target;
	PluginParameter2<double, XMLThreadsafeEvaluator, RequiredPolicy> strength;
	
public:
	InterfaceConstraint();
	DECLARE_PLUGIN("InterfaceConstraint");

	double hamiltonian ( CPM::CELL_ID  cell_id ) const override;
    double delta ( const SymbolFocus& cell_focus, const CPM::Update& update) const override;

};

#endif
