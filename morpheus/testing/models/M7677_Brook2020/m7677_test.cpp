#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
using namespace boost::math;



TEST (MODEL, M7677) {
	ImportFile("model_M7677.xml");
	int replicates = 1;
	
	auto model = TestModel(GetFileString("model_M7677.xml"));
	model.init();
	
	struct P {std::string symbol; double mean; double stddev; };
	std::vector<P> parameters_100 {
		{"P_D", 0.6990170419, 1e-10},
		{"P_S", 0.005168814058, 1e-10},
		{"P_A", 0.2484433693, 1e-10},
		{"P_E", 0.02411102891, 1e-10},
		{"P_I", 0.02315979577, 1e-10},
		{"celltype.dead.size", 6059.1, 77},
		{"celltype.susceptible.size", 9.6, 4.8},
		{"celltype.antiviral.size", 1897.1, 34},
		{"celltype.exposed.size", 918.2, 43},
		{"celltype.infectious.size", 920, 31}
	};
	std::vector<P> parameters_200 {
		{"P_D", 0.8842619523, 1e-10},
		{"P_S", 0.005010625795, 1e-10 },
		{"P_A", 0.08962451566, 1e-10 },
		{"P_E", 0.01062489864, 1e-10 },
		{"P_I", 0.01037805759, 1e-10 },
		{"celltype.dead.size",        8858, 116},
		{"celltype.susceptible.size", 819.4, 88},
		{"celltype.antiviral.size",   19.6, 4.4},
		{"celltype.exposed.size",     53.9, 33},
		{"celltype.infectious.size",  53.1, 24}
	};
	model.run(100);
	
	for (const auto& p : parameters_100) {
		EXPECT_NEAR( SIM::findGlobalSymbol<double>(p.symbol)->get(SymbolFocus::global), p.mean, p.stddev*5);
	}
	
// 	model.run(100);
// 	for (const auto& p : parameters_200) {
// 		EXPECT_NEAR( SIM::findGlobalSymbol<double>(p.symbol)->get(SymbolFocus::global), p.mean, p.stddev*5);
// 	}
}
