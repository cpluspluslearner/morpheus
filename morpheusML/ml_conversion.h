#pragma once
#include "xml_functions.h"
#include "morpheusML_rules.h"

namespace MorpheusML {
	struct Edit {
		enum ModelEditType {AttribAdd, AttribRemove, AttribRename, AttribChange,  NodeAdd, NodeRemove, NodeRename, NodeMove, TextChange};
		enum AutoFix::Severty severty;
		string info;
		string name, value;
		ModelEditType edit_type;
		XMLNode xml_parent;
	};
	
	std::vector< MorpheusML::Edit > convert(XMLNode xNode, int version);
}
