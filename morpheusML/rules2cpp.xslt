<?xml version="1.0"?>

<xsl:stylesheet version="1.1"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common"  >
<xsl:output method="text" encoding="UTF-8" indent="no"/>
        
<xsl:param name="operation-map">
	<operation name='copy'>AutoFix::COPY</operation>
	<operation name='move'>AutoFix::MOVE</operation>
	<operation name='delete'>AutoFix::DELETE</operation>
</xsl:param>
<xsl:variable name="operations" select="exsl:node-set($operation-map)/operation"/>

<xsl:param name="severty-map">
	<severty name='silent'>AutoFix::SILENT</severty>
	<severty name='info'>AutoFix::INFO</severty>
	<severty name='check'>AutoFix::CHECK</severty>
	<severty name='warn'>AutoFix::WARN</severty>
	<severty name='error'>AutoFix::ERROR</severty>
</xsl:param>
<xsl:variable name="severties" select="exsl:node-set($severty-map)/severty"/>

<xsl:template match="/Ruleset">
<!--<xsl:text><![CDATA[
vector<AutoFix::Rule> ruleset_]]></xsl:text><xsl:value-of select="@version-from"/>_<xsl:value-of select="@version-to"/>() {-->
rulesets[ {<xsl:value-of select="@version-from"/>,<xsl:value-of select="@version-to"/>} ] = []() {
	<xsl:text><![CDATA[
	AutoFix rule;
	std::vector<AutoFix>rules; ]]></xsl:text>
	<xsl:for-each select="Rule">
	<xsl:variable name="op" select="@operation"/>
	<xsl:variable name="sv" select="@severty"/>
	rule.match_path = "<xsl:value-of select="normalize-space(MatchPath)"/>";
	rule.require_path = "<xsl:value-of select="normalize-space(RequirePath)"/>";
<!--	<xsl:if test="TargetPath != ''">-->
	rule.target_path = "<xsl:value-of select="normalize-space(TargetPath)"/>";
<!-- 	</xsl:if> -->
	<xsl:apply-templates select="ConversionMap" />
	rule.operation = <xsl:value-of select="$operations[@name = $op]"/>;
<!-- 	rule.replace_existing = <xsl:value-of select="if (@replace) then @replace else true" /> -->
	<xsl:variable name="replace">
		<xsl:choose>
			<xsl:when test="@replace">
				<xsl:value-of select="@replace"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>true</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	rule.replace_existing = <xsl:value-of select="$replace"/>;
	rule.severty = <xsl:value-of select="$severties[@name = $sv]"/>;
	rule.message = "<xsl:value-of select="normalize-space(Message)"/>";
	rules.push_back(rule); 
	</xsl:for-each>
	return rules;
}();
</xsl:template>

<xsl:template match="ConversionMap">
	rule.value_conversions = {
	<xsl:for-each select="Value">
		{ "<xsl:value-of select="@from"/>", "<xsl:value-of select="@to"/>"<xsl:text> }</xsl:text>
		<xsl:if test="position()!=last()"> <xsl:text>,</xsl:text> </xsl:if>
	</xsl:for-each>
	};

</xsl:template>

</xsl:stylesheet>
