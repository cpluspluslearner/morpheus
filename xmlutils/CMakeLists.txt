set(xml_util_src 
	string_functions.cpp
	xml_functions.cpp
)


################################
## External Package dependencies
################################

FIND_PACKAGE(Boost REQUIRED)

add_library(XMLUtils ${xml_util_src} )
target_include_directories(XMLUtils PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries_patched(XMLUtils PUBLIC xmlParser Boost::boost)

